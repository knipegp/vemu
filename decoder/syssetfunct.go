package decoder

const (
	// Other includes all not CSR instructions
	Other Funct3 = iota
	// Csrrw is the funct value for csrrw
	Csrrw
	// Csrrs is the funct value for csrrs
	Csrrs
	// Csrrc is the funct value for csrrc
	Csrrc
	_
	// Csrrwi is the funct value for csrrwi
	Csrrwi
	// Csrrsi is the funct value for csrrsi
	Csrrsi
	// Csrrci is the funct value for csrrci
	Csrrci
)
