package decoder

const (
	Flw MinorOpcode = 0x1
	Fsw MinorOpcode = 0x9
)

const (
	FLoadStore Funct3 = 0x2
)
