package decoder_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/knipegp/vemu/decoder"
)

var _ = Describe("Instfields", func() {
	var csrrs, blt, jal, sw, sub decoder.RawInstruction
	BeforeEach(func() {
		csrrs = decoder.NewRawInstruction(uint32(0x3002a073))
		blt = decoder.NewRawInstruction(uint32(0x0002ca63))
		jal = decoder.NewRawInstruction(uint32(0x5940206f))
		sw = decoder.NewRawInstruction(uint32(0xf6a2a623))
		sub = decoder.NewRawInstruction(uint32(0x40f707b3))
	})
	It("Should decode csrrs fields", func() {
		Expect(csrrs.GetImmediate()).To(Equal(decoder.Immediate(0x300)))
		Expect(csrrs.GetRs1()).To(Equal(decoder.RegisterIndex(5)))
		Expect(csrrs.GetRd()).To(Equal(decoder.RegisterIndex(0)))
		Expect(csrrs.GetFunct3()).To(Equal(decoder.Funct3(2)))
		Expect(csrrs.MajOp).To(Equal(decoder.MajorOpcode(3)))
		Expect(csrrs.GetMinorOpcode()).To(Equal(decoder.MinorOpcode(0x1c)))
	})
	It("Should decode blt fields", func() {
		Expect(blt.GetImmediate()).To(Equal(decoder.Immediate(20)))
		Expect(blt.GetRs2()).To(Equal(decoder.RegisterIndex(0)))
	})
	It("Should decode jal fields", func() {
		Expect(jal.GetImmediate()).To(Equal(decoder.Immediate(9620)))
	})
	It("Should decode sw fields", func() {
		Expect(sw.GetImmediate()).
			To(Equal(decoder.Immediate(int64(-148) & 0xfff)))
	})
	It("Should decode sub fields", func() {
		Expect(sub.GetFunct7()).To(Equal(decoder.Funct7(32)))
	})
})
