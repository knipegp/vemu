package decoder_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestDecoder(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Decoder Suite")
}
