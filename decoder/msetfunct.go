package decoder

const (
	// MulDiv denotes an M extension instruction.
	MulDiv Funct7 = 1
)

const (
	// Mul is a multiply instruction and sets the lower bits of the product.
	Mul Funct3 = iota
	// Mulh is a multiply instruction and sets the upper bits of the product.
	Mulh
	// Mulhsu is a multiply instruction, returns upper bits, rs1 signed, rs2
	// unsigned.
	Mulhsu
	// Mulhu is a multiply instruction, returns upper bits, both multiplicands
	// are unsigned.
	Mulhu
	// Div is a divide instruction.
	Div
	// Divu is an unsigned division instruction.
	Divu
	// Rem returns the remainder of division.
	Rem
	// Remu returns the remainder of unsigned division.
	Remu
)
