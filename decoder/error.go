package decoder

import "fmt"

// TranslationRegistrationError indicates that a problem occurred while
// registering instruction ID to Uop translations in the Decoder.
type TranslationRegistrationError struct {
	str string
}

func NewTransRegError(insn string, id uint32, mask uint32, duplicate bool) TranslationRegistrationError {
	if duplicate {
		return TranslationRegistrationError{
			str: fmt.Sprintf("attempted to register %s at duplicate ID %#x", insn, id),
		}
	}
	return TranslationRegistrationError{
		str: fmt.Sprintf("instruction, %s, is not instantiated correctly; ID %#x mask %#x", insn, id, mask),
	}
}

func (t TranslationRegistrationError) Error() string {
	return t.str
}
