package decoder

const (
	// Beq if equal
	Beq Funct3 = iota
	// Bne not equal
	Bne
	_
	_
	// Blt less than
	Blt
	// Bge greater than or equal
	Bge
	// Bltu less than unsigned
	Bltu
	// Bgeu greater than or equal unsigned
	Bgeu
)

const (
	// Addi add immediate
	Addi Funct3 = iota
	// Slli shift left logical immediate
	Slli
	// Slti set less than immediate
	Slti
	// Sltiu set less than immediate unsigned
	Sltiu
	// Xori xor immediate
	Xori
	// Sr logical and arithmetic
	Sr
	// Ori or immediate
	Ori
	// Andi and immediate
	Andi
)

const (
	// RegDiff is add and sub funct3 for reg-reg operations
	RegDiff Funct3 = iota
	// Sll shift left logical
	Sll
	// Slt set less than
	Slt
	// Sltu set less than unsigned
	Sltu
	// Xor xor
	Xor
	// RegShift denotes shift operations on reg-reg operands
	RegShift
	// Or or
	Or
	// And and
	And
)

const (
	// Add is an add reg-reg instruction
	Add Funct7 = 0
	// Sub is a sub reg-reg instruction
	Sub Funct7 = 32
	// Srl is a shift right logical reg-reg instruction
	Srl Funct7 = 0
	// Sra is a shift right arithmetic reg-reg instruction
	Sra Funct7 = 32
)

const (
	// Lb load byte
	Lb Funct3 = iota
	// Lh load half
	Lh
	// Lw load word
	Lw
	// Ld load double
	Ld
	// Lbu Load byte unsigned
	Lbu
	// Lhu load half unsigned
	Lhu
	// Lwu load word unsigned
	Lwu
)

const (
	// Sb store byte
	Sb Funct3 = iota
	// Sh store half
	Sh
	// Sw store word
	Sw
	// Sd store double
	Sd
)

const (
	// Fence fence
	Fence Funct3 = iota
	// Fencei fencei
	Fencei
)
