package decoder

type (
	// MajorOpcode determines whether the instruction is part of the G designation
	MajorOpcode uint32
	// MinorOpcode determines the format of the given instruction
	MinorOpcode uint32
	// RegisterIndex determines the indexes the register in the register file
	RegisterIndex uint32
	// Immediate represents an integer value in binary
	Immediate uint32
)

const (
	// C0 designates a compact instruction in the C0 space.
	C0 MajorOpcode = iota
	// C1 designates a compact instruction in the C1 space.
	C1
	// C2 designates a compact instruction in the c2 space.
	C2
	// Std designates a full length instruction that is part of the standard ISA.
	Std
)

const (
	// IntRegReg designates an integer register-register instruction
	IntRegReg MinorOpcode = 0xc
	// IntRegImm designates an integer register-immediate instruction
	IntRegImm MinorOpcode = 0x4
	// IntRegReg32 designates an integer register-register instruction in RV64 for a 32-bit operation.
	IntRegReg32 MinorOpcode = 0xe
	// IntRegImm32 designates an integer register-immediate instruction in RV64 for a 32-bit operation.
	IntRegImm32 MinorOpcode = 0x6
	// Load designates a load instruction
	Load MinorOpcode = 0x0
	// Jalr designates a JALR instruction
	Jalr MinorOpcode = 0x19
	// Store designates a store type instruction
	Store MinorOpcode = 0x8
	// Branch desigtnates a branch type instruction
	Branch MinorOpcode = 0x18
	// Lui designates an LUI instruction
	Lui MinorOpcode = 0xd
	// Auipc designates an AUIPC instruction
	Auipc MinorOpcode = 0x5
	// Jal designates a JAL instruction
	Jal MinorOpcode = 0x1b
	// FenceOp designates a fence instruction
	FenceOp MinorOpcode = 0x3
	// Sys designates a system call instruction
	Sys MinorOpcode = 0x1c
	// Atomic designates an atomic instruction
	Atomic MinorOpcode = 0xb
)
