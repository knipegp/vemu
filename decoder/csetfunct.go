package decoder

const (
	// Caddi4spn is a add immediate, scaled 4, to SP.
	Caddi4spn Funct3 = iota
	// Cfld compact load float double
	Cfld
	// Clw compact load word
	Clw
	// Cflw compact float load word
	Cflw
	_
	// Cfsd compact float store double
	Cfsd
	// Csw compact store word
	Csw
	// Cfsw compact float store word
	Cfsw
)

const (
	// Caddi compact add immediate
	Caddi Funct3 = iota
	// Cjal compact jump and link
	Cjal
	// Cli compact load immediate.
	Cli
	// Clui compact load upper immediate
	Clui
	// Extend6 designates a funct field that extends to Funct6
	Extend6
	// Cj compact jump
	Cj
	// Cbeqz compact branch equal to zero
	Cbeqz
	// Cbnez compact branch not equal to zero
	Cbnez
)

const (
	// Csrli compact shift right logical immediate
	Csrli Funct6Tail = iota
	// Csrai compact shift right arithmetic immediate
	Csrai
	// Candi compact and immediate
	Candi
	// RegGroupTail is a group of compact arithmetic register instructions
	RegGroupTail
)

const (
	// RegGroupMid is a group of compact arithmetic register instructions
	RegGroupMid Bit12 = iota
)

const (
	// Csub compact subtract
	Csub Funct2 = iota
	// Cxor compact Xor
	Cxor
	// Cor compact Or
	Cor
	// Cand compact and
	Cand
)

const (
	// Cslli compact shift left logical
	Cslli Funct3 = iota
	// Cfldsp compact float load double stack pointer
	Cfldsp
	// Clwsp compact load double stack pointer
	Clwsp
	// Cflwsp compact float load word stack pointer
	Cflwsp
	// CMvAdd contains the move and add instructions
	CMvAdd
	// Cfsdsp compact float store double stack pointer
	Cfsdsp
	// Cswsp compact store word stack pointer
	Cswsp
	// Cfswsp compact float store word stack pointer
	Cfswsp
)

const (
	// Cmv compact move
	Cmv Bit12 = iota
	// Cadd compact add
	Cadd
)
