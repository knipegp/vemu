package decoder

import (
	"fmt"

	"gitlab.com/knipegp/vemu/bits"
)

func majOp(word uint32) MajorOpcode {
	return MajorOpcode(bits.GetBits(uint64(word), 1, 0))
}

// RawInstruction enables field extraction from an instruction word
type RawInstruction struct {
	BinWord uint32
	MajOp   MajorOpcode
}

// NewRawInstruction assigns the binary word to the structure.
func NewRawInstruction(bits uint32) RawInstruction {
	majOp := majOp(bits)
	var instWord uint32
	if majOp < Std {
		instWord = bits & 0xffff
	} else {
		instWord = bits
	}
	return RawInstruction{instWord, majOp}
}

func (r RawInstruction) getBits(upperBound int, lowerBound int) uint64 {
	return bits.GetBits(uint64(r.BinWord), upperBound, lowerBound)
}

// GetRd returns the rd field for the instruction word
func (r RawInstruction) GetRd() RegisterIndex {
	return RegisterIndex(r.getBits(11, 7))
}

// GetMinorOpcode returns the minor opcode for the given instruction
func (r RawInstruction) GetMinorOpcode() MinorOpcode {
	return MinorOpcode(r.getBits(6, 2))
}

// GetFunct3 returns the funct3 field
func (r RawInstruction) GetFunct3() Funct3 {
	return Funct3(r.getBits(14, 12))
}

// GetFunct7 returns the funct7 field
func (r RawInstruction) GetFunct7() Funct7 {
	return Funct7(r.getBits(31, 25))
}

// GetRs3 returns the rs2 field
func (r RawInstruction) GetRs3() RegisterIndex {
	return RegisterIndex(r.getBits(31, 27))
}

// GetRs2 returns the rs2 field
func (r RawInstruction) GetRs2() RegisterIndex {
	return RegisterIndex(r.getBits(24, 20))
}

// GetRs1 returns the rs1 field
func (r RawInstruction) GetRs1() RegisterIndex {
	return RegisterIndex(r.getBits(19, 15))
}

// CFunct3 returns a compact Funct3 field
func (r RawInstruction) CFunct3() Funct3 {
	return Funct3(r.getBits(15, 13))
}

// CRdRs1 returns a compact rd/rs1 field
func (r RawInstruction) CRdRs1() RegisterIndex {
	return RegisterIndex(r.getBits(11, 7))
}

// CRs2 returns a compact rs2 field
func (r RawInstruction) CRs2() RegisterIndex {
	return RegisterIndex(r.getBits(6, 2))
}

// CLWSPImm returns CLWSP or CFLWSP immediate.
func (r RawInstruction) CLWSPImm() Immediate {
	imm := r.getBits(6, 4) << 2
	imm |= r.getBits(12, 12) << 5
	imm |= r.getBits(3, 2) << 6
	return Immediate(imm)
}

// CLDSPImm returns CLDSP or CFLDSP immediate.
func (r RawInstruction) CLDSPImm() Immediate {
	imm := r.getBits(6, 5) << 3
	imm |= r.getBits(12, 12) << 5
	imm |= r.getBits(4, 2) << 6
	return Immediate(imm)
}

// CLQSPImm returns CLQSPImm immediate.
func (r RawInstruction) CLQSPImm() Immediate {
	imm := r.getBits(6, 6) << 4
	imm |= r.getBits(12, 12) << 5
	imm |= r.getBits(5, 2) << 6
	return Immediate(imm)
}

// CSWSPImm returns CSWSP or CFSWSP immediate.
func (r RawInstruction) CSWSPImm() Immediate {
	imm := r.getBits(12, 9) << 2
	imm |= r.getBits(8, 7) << 6
	return Immediate(imm)
}

// CSDSPImm returns CSDSP or CFSDSP immediate.
func (r RawInstruction) CSDSPImm() Immediate {
	imm := r.getBits(12, 10) << 3
	imm |= r.getBits(9, 7) << 6
	return Immediate(imm)
}

// CSQSPImm returns CSQSP immediate.
func (r RawInstruction) CSQSPImm() Immediate {
	imm := r.getBits(12, 11) << 4
	imm |= r.getBits(10, 7) << 6
	return Immediate(imm)
}

// CWImm returns CLW, CFLW, CSW, and CFSW immediate.
func (r RawInstruction) CWImm() Immediate {
	imm := r.getBits(12, 10) << 3
	imm |= r.getBits(6, 6) << 2
	imm |= r.getBits(5, 5) << 6
	return Immediate(imm)
}

// CDImm returns CLD, CFLD, CSD, and CFSD immediate.
func (r RawInstruction) CDImm() Immediate {
	imm := r.getBits(12, 10) << 3
	imm |= r.getBits(6, 5) << 6
	return Immediate(imm)
}

// CQImm returns CLQ and CSQ immediate.
func (r RawInstruction) CQImm() Immediate {
	imm := r.getBits(12, 11) << 4
	imm |= r.getBits(10, 10) << 8
	imm |= r.getBits(6, 5) << 6
	return Immediate(imm)
}

// CJImm returns the compact j-type immediate.
func (r RawInstruction) CJImm() Immediate {
	imm := r.getBits(12, 12) << 11
	imm |= r.getBits(11, 11) << 4
	imm |= r.getBits(10, 9) << 8
	imm |= r.getBits(8, 8) << 10
	imm |= r.getBits(7, 7) << 6
	imm |= r.getBits(6, 6) << 7
	imm |= r.getBits(5, 3) << 1
	imm |= r.getBits(2, 2) << 5
	return Immediate(imm)
}

// CBImm returns the compact b-type immediate.
func (r RawInstruction) CBImm() Immediate {
	imm := r.getBits(12, 12) << 8
	imm |= r.getBits(11, 10) << 3
	imm |= r.getBits(6, 5) << 6
	imm |= r.getBits(4, 3) << 1
	imm |= r.getBits(2, 2) << 5
	return Immediate(imm)
}

// CIImm returns the CLI, CADDI, CSLLI, CSRLI, CSRAI, CANDI, and CADDIW immediate.
func (r RawInstruction) CIImm() Immediate {
	imm := r.getBits(12, 12) << 5
	imm |= r.getBits(6, 2)
	return Immediate(imm)
}

// CLUIImm returns the CLUI immediate.
func (r RawInstruction) CLUIImm() Immediate {
	imm := r.getBits(12, 12) << 17
	imm |= r.getBits(6, 2) << 12
	return Immediate(imm)
}

// CADDI16Imm returns the CADDI16SP immediate.
func (r RawInstruction) CADDI16Imm() Immediate {
	imm := r.getBits(12, 12) << 9
	imm |= r.getBits(6, 6) << 4
	imm |= r.getBits(5, 5) << 6
	imm |= r.getBits(4, 3) << 7
	imm |= r.getBits(2, 2) << 5
	return Immediate(imm)
}

// CADDI4Imm returns the CADDI4SPN immediate.
func (r RawInstruction) CADDI4Imm() Immediate {
	imm := r.getBits(12, 11) << 4
	imm |= r.getBits(10, 7) << 6
	imm |= r.getBits(6, 6) << 2
	imm |= r.getBits(5, 5) << 3
	return Immediate(imm)
}

// CRdRs1Short returns the shortened Rd'Rs1' field.
func (r RawInstruction) CRdRs1Short() RegisterIndex {
	return RegisterIndex(r.getBits(9, 7) + 8)
}

// CBFunct2 returns the compact funct2 field for cb-type instructions.
func (r RawInstruction) CBFunct2() Funct2 {
	return Funct2(r.getBits(11, 10))
}

// CAFunct2 returns the compact funct2 field for the ca-type instructions.
func (r RawInstruction) CAFunct2() Funct2 {
	return Funct2(r.getBits(6, 5))
}

// CRdShort returns the compact Rd field.
func (r RawInstruction) CRdShort() RegisterIndex {
	return RegisterIndex(r.getBits(4, 2) + 8)
}

// CRs2Short returns the compact short Rs2 field.
func (r RawInstruction) CRs2Short() RegisterIndex {
	return RegisterIndex(r.getBits(4, 2) + 8)
}

// CRs1 returns the compact Rs1 field.
func (r RawInstruction) CRs1() RegisterIndex {
	return RegisterIndex(r.getBits(9, 7))
}

// TODO: Break separate immediate methods for each instruction format.

// GetImmediate returns the immediate value encoded in the instruction
func (r RawInstruction) GetImmediate() Immediate {
	var imm Immediate
	minorOp := r.GetMinorOpcode()
	switch minorOp {
	case IntRegImm, IntRegImm32, Sys, Load, Jalr, Flw, MinorOpcode(0x3):
		imm = Immediate(r.getBits(31, 20))
	case Store, MinorOpcode(0x9):
		lowerBits := r.getBits(11, 7)
		upperBits := r.getBits(31, 25)
		imm = Immediate((upperBits << 5) | lowerBits)
	case Branch:
		bit12 := r.getBits(31, 31)
		bit11 := r.getBits(7, 7)
		upperBits := r.getBits(30, 25)
		lowerBits := r.getBits(11, 8)
		imm = Immediate((bit12 << 12) | (bit11 << 11) | (upperBits << 5) | (lowerBits << 1))
	case Lui, Auipc:
		imm = Immediate(r.getBits(31, 12) << 12)
	case Jal:
		bit20 := r.getBits(31, 31)
		lowerBits := r.getBits(30, 21)
		bit11 := r.getBits(20, 20)
		upperBits := r.getBits(19, 12)
		imm = Immediate((bit20 << 20) | (upperBits << 12) | (bit11 << 11) | (lowerBits << 1))
	default:
		panic(fmt.Errorf("instruction does not have immediate, minop: 0x%x", r.GetMinorOpcode()))
	}
	return imm
}
