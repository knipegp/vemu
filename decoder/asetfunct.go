package decoder

const (
	// Atomic32 denotes a 32-bit atomic instruction.
	Atomic32 Funct3 = 2
	// Atomic64 denotes a 64-bit atomic instruction.
	Atomic64 Funct3 = 3
)

const (
	CommonAtomic Funct5Head = iota
	Amoxor
	Amoor
	Amoand
	Amomin
	Amomax
	Amominu
	Amomaxu
)

const (
	ArithAtomic Funct5Tail = iota
	Amoswap
	Lr
	Sc
)
