package decoder

type (
	// Funct3 determines the operation for an instruction
	Funct3 uint32
	// Funct5Head determines the operation for an atomic instruction.
	Funct5Head uint32
	// Funct5Tail determines the operation for an atomic instruction.
	Funct5Tail uint32
	// Funct7 determines the operation for an R format instruction
	Funct7 uint32
	// Bit12 is part of Func4 and Funct6 fields.
	Bit12 uint32
	// Funct2 determines the operation for some compact instructions
	Funct2 uint32
	// Funct6Tail is a funct field extension for the Funct6 field
	Funct6Tail uint32
	// Funct12 is the last 12 bits of SYSTEM instructions
	Funct12 uint32
)

const (
	minOpPart       uint32 = 0x7c
	majOpPart       uint32 = 0x3
	funct5HeadPart  uint32 = 0xe0000000
	funct5TailPart  uint32 = 0x18000000
	stdFunct3Part   uint32 = 0x7000
	stdFunct7Part   uint32 = 0xfe000000
	cFunct2Part     uint32 = 0x60
	cFunct3Part     uint32 = 0xe000
	cFunct6TailPart uint32 = 0xc00
	cBit12Part      uint32 = 0x1000
)

const (
	stdOpMask       uint32 = minOpPart | majOpPart
	stdFunct3Mask   uint32 = stdOpMask | stdFunct3Part
	stdFunct5Mask   uint32 = stdFunct3Mask | funct5HeadPart | funct5TailPart
	stdFunct7Mask   uint32 = stdFunct3Mask | stdFunct7Part
	cFunct6Mask     uint32 = majOpPart | cFunct2Part | cFunct6TailPart | cBit12Part | cFunct3Part
	cFunct6TailMask uint32 = majOpPart | cFunct6TailPart | cFunct3Part
	cFunct4Mask     uint32 = majOpPart | cBit12Part | cFunct3Part
	cFunct3Mask     uint32 = majOpPart | cFunct3Part
)

func validMasks() []uint32 {
	return []uint32{stdOpMask,
		stdFunct3Mask,
		stdFunct5Mask,
		stdFunct7Mask,
		cFunct6Mask,
		cFunct6TailMask,
		cFunct4Mask,
		cFunct3Mask}
}

func isValidMask(mask uint32) bool {
	masks := validMasks()
	for _, entry := range masks {
		if entry == mask {
			return true
		}
	}
	return false
}

func assertValidMask(mask uint32) {
	if !isValidMask(mask) {
		panic("invalid mask")
	}
}

// A valid id cannot have overlapping masks
func hasOverlap(curr uint32, add uint32) bool {
	return curr&add > 0
}

func assertNoOverlap(curr uint32, add uint32) {
	if hasOverlap(curr, add) {
		panic("bad ID construction")
	}
}

// IDBuilder generates an instruction ID and Mask.
//
// To be used at Vemulator build time. Too slow to use during RISC-V binary
// emulation.
type IDBuilder struct {
	id     uint32
	mask   uint32
	custom bool
}

// ID returns the generated instruction ID.
func (i IDBuilder) ID() uint32 {
	return i.id
}

// Mask returns the generated instruction mask used for matching instruction
// words.
func (i IDBuilder) Mask() uint32 {
	// TODO: The fact that I have to do this shows that this is a bad idea.
	if !i.custom {
		assertValidMask(i.mask)
	}
	return i.mask
}

func (i *IDBuilder) update(id uint32, mask uint32) {
	assertNoOverlap(i.mask, mask)
	if id&mask != id {
		panic("invalid mask, id pair")
	}
	i.mask |= mask
	i.id |= id
}

// WithStdFunct3 updates the ID and mask with the specified field.
func (i IDBuilder) WithStdFunct3(funct3 Funct3) IDBuilder {
	i.update(uint32(funct3)<<12, stdFunct3Part)
	return i
}

// WithStdFunct5Head updates the ID and mask with the specified field.
func (i IDBuilder) WithStdFunct5Head(funct5Head Funct5Head) IDBuilder {
	i.update(uint32(funct5Head)<<29, funct5HeadPart)
	return i
}

// WithStdFunct5Tail updates the ID and mask with the specified field.
func (i IDBuilder) WithStdFunct5Tail(funct5Tail Funct5Tail) IDBuilder {
	i.update(uint32(funct5Tail)<<27, funct5TailPart)
	return i
}

// WithMajOp updates the ID and mask with the specified field.
func (i IDBuilder) WithMajOp(majOp MajorOpcode) IDBuilder {
	i.update(uint32(majOp), majOpPart)
	return i
}

// WithMinOp updates the ID and mask with the specified field.
func (i IDBuilder) WithMinOp(minOp MinorOpcode) IDBuilder {
	i.update(uint32(minOp)<<2, minOpPart)
	return i
}

// WithFunct7 updates the ID and mask with the specified field.
func (i IDBuilder) WithFunct7(funct7 Funct7) IDBuilder {
	i.update(uint32(funct7)<<25, stdFunct7Part)
	return i
}

// WithCFunct3 updates the ID and mask with the specified field.
func (i IDBuilder) WithCFunct3(funct3 Funct3) IDBuilder {
	i.update(uint32(funct3)<<13, cFunct3Part)
	return i
}

// WithCBit12 updates the ID and mask with the specified field.
func (i IDBuilder) WithCBit12(bit12 Bit12) IDBuilder {
	i.update(uint32(bit12)<<12, cBit12Part)
	return i
}

// WithCFunct2 updates the ID and mask with the specified field.
func (i IDBuilder) WithCFunct2(funct2 Funct2) IDBuilder {
	i.update(uint32(funct2)<<5, cFunct2Part)
	return i
}

// WithCFunct6Tail updates the ID and mask with the specified field.
func (i IDBuilder) WithCFunct6Tail(funct6tail Funct6Tail) IDBuilder {
	i.update(uint32(funct6tail)<<10, cFunct6TailPart)
	return i
}

// WithCustom adds custom match and mask bits.
//
// The match bits should be normalized such that the LSB is zero bit.
func (i IDBuilder) WithCustom(match uint32, mask uint32) IDBuilder {
	shift := 0
	tmp := mask
	i.custom = true
	for ; tmp&1 == 0; shift, tmp = shift+1, tmp>>1 {
	}
	if tmp|match != tmp {
		panic("match, tmp pair is not valid")
	}
	i.update(match<<shift, mask)
	return i
}
