package riscv

// Xlen denotes the integer register bit length.
type Xlen uint

const (
	// Bit32 denotes a 32-bit register length.
	Bit32 Xlen = 32
	// Bit64 denotes a 64-bit register length.
	Bit64 Xlen = 64
)
