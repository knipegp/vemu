#ifndef __TEST_MACROS_H_
#define __TEST_MACROS_H_

#include "../riscv-tests/isa/macros/scalar/test_macros.h"

#ifdef TEST_PASSFAIL
    #undef TEST_PASSFAIL
#endif // TEST_PASSFAIL
// Return code should already be in a0
#define TEST_PASSFAIL \
pass: \
    li a0, 0; \
    add sp, zero, t3; \
    j exit; \
fail: \
    add a0, zero, gp; \
    add sp, zero, t3; \
    j exit \

#endif // __TEST_MACROS_H_
