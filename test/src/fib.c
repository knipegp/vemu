#include <stdio.h>

int fib(int ordinal) {
    if (ordinal <= 1) {
        return ordinal;
    }
	int prev = 0;
	int current = 1;
	for(int i = 2; i <= ordinal; i++){
		int next = current + prev;
		prev = current;
		current = next;
	}
	return current;
}


int main() {
    return !(fib(46) == 1836311903);
}
