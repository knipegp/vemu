int bench(int ordinal) {
    if (ordinal == 0) {
        return 0;
    } else {
        return ordinal + bench(ordinal-1);
    }
}


int main() {
	return bench(1048576);
}
	

