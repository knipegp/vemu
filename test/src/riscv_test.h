#ifndef __RISCV_TEST_H_
#define __RISCV_TEST_H_

#include "../riscv-tests/env/p/riscv_test.h"

#ifdef RVTEST_CODE_BEGIN
    #undef RVTEST_CODE_BEGIN
#endif // RVTEST_CODE_BEGIN
#define STKSHIFT 17
#define RVTEST_CODE_BEGIN \
        .section .text.init;                                            \
        .align  6;                                                      \
        .weak stvec_handler;                                            \
        .weak mtvec_handler;                                            \
        .globl _start;                                                  \
_start:;                                                                 \
        la  tp, _end + 63; \
        and tp, tp, -64; \
        csrr a0, mhartid; \
        add sp, a0, 1; \
        sll sp, sp, STKSHIFT; \
        add sp, sp, tp; \
        sll a2, a0, STKSHIFT; \
        add tp, tp, a2; \
        add t3, zero, sp \

#endif // __RISCV_TEST_H_
