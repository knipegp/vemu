package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/mem"
	"gitlab.com/knipegp/vemu/exec"
	"gitlab.com/knipegp/vemu/riscv"
	"gitlab.com/knipegp/vemu/vemulator"
)

// getInstrFromDump retrieves the decoded instruction from an objdump
func getInstrFromDump(insnBits uint32, filePath string) string {
	fileHandle, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	fileScanner := bufio.NewScanner(fileHandle)
	for fileScanner.Scan() {
		if strings.Contains(fileScanner.Text(), fmt.Sprintf("%x", insnBits)) {
			return fileScanner.Text()
		}
	}
	return "instruction not found"
}

var _ = Describe("Isa", func() {
	DescribeTable("execute all ISA unit tests", func(path string, xlen riscv.Xlen) {
		vemu := vemulator.Builder{}.
			WithXlen(xlen).
			WithElf(path).
			WithLog(false).
			WithMemCap(4 * mem.GB).
			WithCoreCount(1).
			Build()
		rv, err := vemu.Run()
		Expect(rv).To(Equal(0))
		Expect(err).To(BeNil(), func() string {
			return fmt.Sprintf(
				"%18s failed on:\t%s\n",
				path,
				getInstrFromDump(err.(*exec.InsnMissing).Bits, path+".dump"),
			)
		})

	},
		Entry("rv32ui-p-add", "./rv32ui-p-add", riscv.Bit32),
		Entry("rv32ui-p-addi", "./rv32ui-p-addi", riscv.Bit32),
		Entry("rv32ui-p-and", "./rv32ui-p-and", riscv.Bit32),
		Entry("rv32ui-p-andi", "./rv32ui-p-andi", riscv.Bit32),
		Entry("rv32ui-p-auipc", "./rv32ui-p-auipc", riscv.Bit32),
		Entry("rv32ui-p-beq", "./rv32ui-p-beq", riscv.Bit32),
		Entry("rv32ui-p-bge", "./rv32ui-p-bge", riscv.Bit32),
		Entry("rv32ui-p-bgeu", "./rv32ui-p-bgeu", riscv.Bit32),
		Entry("rv32ui-p-blt", "./rv32ui-p-blt", riscv.Bit32),
		Entry("rv32ui-p-bltu", "./rv32ui-p-bltu", riscv.Bit32),
		Entry("rv32ui-p-bne", "./rv32ui-p-bne", riscv.Bit32),

		// Entry("rv32ui-p-fence_i", "./rv32ui-p-fence_i", riscv.Bit32),

		Entry("rv32ui-p-jal", "./rv32ui-p-jal", riscv.Bit32),
		Entry("rv32ui-p-jalr", "./rv32ui-p-jalr", riscv.Bit32),
		Entry("rv32ui-p-lb", "./rv32ui-p-lb", riscv.Bit32),
		Entry("rv32ui-p-lbu", "./rv32ui-p-lbu", riscv.Bit32),
		Entry("rv32ui-p-lh", "./rv32ui-p-lh", riscv.Bit32),
		Entry("rv32ui-p-lhu", "./rv32ui-p-lhu", riscv.Bit32),
		Entry("rv32ui-p-lui", "./rv32ui-p-lui", riscv.Bit32),
		Entry("rv32ui-p-lw", "./rv32ui-p-lw", riscv.Bit32),
		Entry("rv32ui-p-or", "./rv32ui-p-or", riscv.Bit32),
		Entry("rv32ui-p-ori", "./rv32ui-p-ori", riscv.Bit32),
		Entry("rv32ui-p-sb", "./rv32ui-p-sb", riscv.Bit32),
		Entry("rv32ui-p-sh", "./rv32ui-p-sh", riscv.Bit32),

		// Entry("rv32ui-p-simple", "./rv32ui-p-simple", riscv.Bit32),

		Entry("rv32ui-p-sll", "./rv32ui-p-sll", riscv.Bit32),
		Entry("rv32ui-p-slli", "./rv32ui-p-slli", riscv.Bit32),
		Entry("rv32ui-p-slt", "./rv32ui-p-slt", riscv.Bit32),
		Entry("rv32ui-p-slti", "./rv32ui-p-slti", riscv.Bit32),
		Entry("rv32ui-p-sltiu", "./rv32ui-p-sltiu", riscv.Bit32),
		Entry("rv32ui-p-sltu", "./rv32ui-p-sltu", riscv.Bit32),
		Entry("rv32ui-p-sra", "./rv32ui-p-sra", riscv.Bit32),
		Entry("rv32ui-p-srai", "./rv32ui-p-srai", riscv.Bit32),
		Entry("rv32ui-p-srl", "./rv32ui-p-srl", riscv.Bit32),
		Entry("rv32ui-p-srli", "./rv32ui-p-srli", riscv.Bit32),
		Entry("rv32ui-p-sub", "./rv32ui-p-sub", riscv.Bit32),
		Entry("rv32ui-p-sw", "./rv32ui-p-sw", riscv.Bit32),
		Entry("rv32ui-p-xor", "./rv32ui-p-xor", riscv.Bit32),
		Entry("rv32ui-p-xori", "./rv32ui-p-xori", riscv.Bit32),
		Entry("rv32uc-p-rvc", "./rv32uc-p-rvc", riscv.Bit32),
		Entry("rv64ui-p-add", "./rv64ui-p-add", riscv.Bit64),
		Entry("rv64ui-p-addi", "./rv64ui-p-addi", riscv.Bit64),
		Entry("rv64ui-p-addiw", "./rv64ui-p-addiw", riscv.Bit64),
		Entry("rv64ui-p-addw", "./rv64ui-p-addw", riscv.Bit64),
		Entry("rv64ui-p-and", "./rv64ui-p-and", riscv.Bit64),
		Entry("rv64ui-p-andi", "./rv64ui-p-andi", riscv.Bit64),
		Entry("rv64ui-p-auipc", "./rv64ui-p-auipc", riscv.Bit64),
		Entry("rv64ui-p-beq", "./rv64ui-p-beq", riscv.Bit64),
		Entry("rv64ui-p-bge", "./rv64ui-p-bge", riscv.Bit64),
		Entry("rv64ui-p-bgeu", "./rv64ui-p-bgeu", riscv.Bit64),
		Entry("rv64ui-p-blt", "./rv64ui-p-blt", riscv.Bit64),
		Entry("rv64ui-p-bltu", "./rv64ui-p-bltu", riscv.Bit64),
		Entry("rv64ui-p-bne", "./rv64ui-p-bne", riscv.Bit64),

		// Entry("rv64ui-p-fence_i", "./rv64ui-p-fence_i", riscv.Bit64),

		Entry("rv64ui-p-jal", "./rv64ui-p-jal", riscv.Bit64),
		Entry("rv64ui-p-jalr", "./rv64ui-p-jalr", riscv.Bit64),
		Entry("rv64ui-p-lb", "./rv64ui-p-lb", riscv.Bit64),
		Entry("rv64ui-p-lbu", "./rv64ui-p-lbu", riscv.Bit64),
		Entry("rv64ui-p-ld", "./rv64ui-p-ld", riscv.Bit64),
		Entry("rv64ui-p-lh", "./rv64ui-p-lh", riscv.Bit64),
		Entry("rv64ui-p-lhu", "./rv64ui-p-lhu", riscv.Bit64),
		Entry("rv64ui-p-lui", "./rv64ui-p-lui", riscv.Bit64),
		Entry("rv64ui-p-lw", "./rv64ui-p-lw", riscv.Bit64),
		Entry("rv64ui-p-lwu", "./rv64ui-p-lwu", riscv.Bit64),
		Entry("rv64ui-p-or", "./rv64ui-p-or", riscv.Bit64),
		Entry("rv64ui-p-ori", "./rv64ui-p-ori", riscv.Bit64),
		Entry("rv64ui-p-sb", "./rv64ui-p-sb", riscv.Bit64),
		Entry("rv64ui-p-sd", "./rv64ui-p-sd", riscv.Bit64),
		Entry("rv64ui-p-sh", "./rv64ui-p-sh", riscv.Bit64),

		// Entry("rv64ui-p-simple", "./rv64ui-p-simple", riscv.Bit64),

		Entry("rv64ui-p-sll", "./rv64ui-p-sll", riscv.Bit64),
		Entry("rv64ui-p-slli", "./rv64ui-p-slli", riscv.Bit64),
		Entry("rv64ui-p-slliw", "./rv64ui-p-slliw", riscv.Bit64),
		Entry("rv64ui-p-sllw", "./rv64ui-p-sllw", riscv.Bit64),
		Entry("rv64ui-p-slt", "./rv64ui-p-slt", riscv.Bit64),
		Entry("rv64ui-p-slti", "./rv64ui-p-slti", riscv.Bit64),
		Entry("rv64ui-p-sltiu", "./rv64ui-p-sltiu", riscv.Bit64),
		Entry("rv64ui-p-sltu", "./rv64ui-p-sltu", riscv.Bit64),
		Entry("rv64ui-p-sra", "./rv64ui-p-sra", riscv.Bit64),
		Entry("rv64ui-p-srai", "./rv64ui-p-srai", riscv.Bit64),
		Entry("rv64ui-p-sraiw", "./rv64ui-p-sraiw", riscv.Bit64),
		Entry("rv64ui-p-sraw", "./rv64ui-p-sraw", riscv.Bit64),
		Entry("rv64ui-p-srl", "./rv64ui-p-srl", riscv.Bit64),
		Entry("rv64ui-p-srli", "./rv64ui-p-srli", riscv.Bit64),
		Entry("rv64ui-p-srliw", "./rv64ui-p-srliw", riscv.Bit64),
		Entry("rv64ui-p-srlw", "./rv64ui-p-srlw", riscv.Bit64),
		Entry("rv64ui-p-sub", "./rv64ui-p-sub", riscv.Bit64),
		Entry("rv64ui-p-subw", "./rv64ui-p-subw", riscv.Bit64),
		Entry("rv64ui-p-sw", "./rv64ui-p-sw", riscv.Bit64),
		Entry("rv64ui-p-xor", "./rv64ui-p-xor", riscv.Bit64),
		Entry("rv64ui-p-xori", "./rv64ui-p-xori", riscv.Bit64),
		Entry("rv64um-p-div", "./rv64um-p-div", riscv.Bit64),
		Entry("rv64um-p-divu", "./rv64um-p-divu", riscv.Bit64),
		Entry("rv64um-p-diuw", "./rv64um-p-divuw", riscv.Bit64),
		Entry("rv64um-p-mul", "./rv64um-p-mul", riscv.Bit64),
		Entry("rv64um-p-mulh", "./rv64um-p-mulh", riscv.Bit64),
		Entry("rv64um-p-mulhsu", "./rv64um-p-mulhsu", riscv.Bit64),
		Entry("rv64um-p-mulhu", "./rv64um-p-mulhu", riscv.Bit64),
		Entry("rv64um-p-mulw", "./rv64um-p-mulw", riscv.Bit64),
		Entry("rv64um-p-rem", "./rv64um-p-rem", riscv.Bit64),
		Entry("rv64um-p-remu", "./rv64um-p-remu", riscv.Bit64),
		Entry("rv64um-p-remuw", "./rv64um-p-remuw", riscv.Bit64),
		Entry("rv64um-p-remw", "./rv64um-p-remw", riscv.Bit64),
	)
})
