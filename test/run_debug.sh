#!/bin/bash

set -euf
# From: https://stackoverflow.com/a/246128/8858389
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

BIN="$1"
CFG="$DIR/spike.cfg"

SPIKE="(spike --isa=RV32I --rbb-port=9824 -H $BIN &)"
OPENOCD="openocd -f $CFG &"
GDB="riscv64-unknown-elf-gdb $BIN"

eval "$SPIKE"
eval "$OPENOCD"
eval "$GDB"
