package bits

import "math"

const (
	bit32SignMask uint64 = (uint64(math.MaxUint32) << 1) + 1
)

// GetBits returns the bits within the provided bounds (inclusive). The
// requested bits are shifted to the LSB.
func GetBits(source uint64, upperBound int, lowerBound int) uint64 {
	if lowerBound > upperBound {
		panic("passed upperBound is less than lowerBound")
	}
	leftShift := 63 - upperBound
	out := source << leftShift
	return out >> (leftShift + lowerBound)
}

// Modified from Akita/mgpusim

// SignExt updates all the bits beyond the signBit to be the same as the sign
// bit.
func SignExt(in uint64, signBit int) (out uint64) {
	out = in

	var mask uint64
	mask = ^((1 << (signBit + 1)) - 1)

	sign := (in >> signBit) & 1

	if sign > 0 {
		out = out | mask
	} else {
		mask = ^mask
		out = out & mask
	}

	return out
}

// Is32BitSignExt returns true if the 64-bit value is a sign extended 32-bit
// value.
func Is32BitSignExt(val uint64) (ret bool) {
	check := val >> 31
	if check&bit32SignMask == check {
		ret = true
	} else {
		ret = false
	}
	return
}

func valToByteArr(val uint64, bytes int) [8]byte {
	var arr [8]byte
	for idx := 0; idx < bytes; idx++ {
		arr[idx] = byte(val >> (idx * 8))
	}
	return arr
}

// Uint64ByteArray converts a uint64 to an array of bytes.
//
// The length is determined by the transaction length with memory stores.
func Uint64ByteArray(val uint64) [8]byte {
	return valToByteArr(val, 8)
}

// Uint32ByteArray converts a uint32 to an array of bytes.
//
// The length is determined by the transaction length with memory stores.
func Uint32ByteArray(val uint32) [8]byte {
	return valToByteArr(uint64(val), 4)
}

// Uint16ByteArray converts a uint16 to an array of bytes.
//
// The length is determined by the transaction length with memory stores.
func Uint16ByteArray(val uint16) [8]byte {
	return valToByteArr(uint64(val), 2)
}

// BytesToUint32 converts a byte slice to a uint32.
func BytesToUint32(bytes []byte) uint32 {
	var data uint32
	for idx := uint64(0); idx < 4; idx++ {
		data |= uint32(bytes[idx]) << (idx * 8)
	}
	return data
}
