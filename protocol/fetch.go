package protocol

// FetchRspType indicates the type of fetch response.
type FetchRspType byte

const (
	// EmptyFetchRsp indicates that no response is available.
	EmptyFetchRsp FetchRspType = iota
	// FetchMemRead indicates that the fetch stage must read from higher level memory.
	FetchMemRead
	// FetchCommit indicates that the fetch stage is emitting valid isntruction bits.
	FetchCommit
)

// FetchRsp contains response messages from the fetch stage.
type FetchRsp struct {
	Type   FetchRspType
	Read   MemRead
	Commit ExecCommit
}

// FetchJmp contains the jump PC from which the module should retrieve the next
// instruction bits.
type FetchJmp struct {
	PC uint64
}
