package protocol

// MStatus represents the MStatus output from a CSR stage.
// From: rocket-chip/src/main/scala/rocket/CSR.scala
type MStatus struct {
	// not truly part of mstatus, but convenient
	Debug bool
	Cease bool
	WFI   bool
	ISA   ExtensionConfig

	DPrv   uint8 // effective privilege for data accesses
	Prv    uint8 // not truly part of mstatus, but convenient
	SU     bool
	Zero2  uint32
	SXl    uint8
	UXl    uint8
	SDRV32 bool
	Zero1  uint8
	TSR    bool
	TW     bool
	TVM    bool
	MXR    bool
	Sum    bool
	MPrv   bool
	XS     uint8
	FS     uint8
	MPP    uint8
	VS     uint8
	SPP    bool
	MPIE   bool
	HPIE   bool
	SPIE   bool
	UPIE   bool
	MIE    bool
	HIE    bool
	SIE    bool
	UIE    bool
}

// CSRDecodeIO represents direct communication between the CSR and Decode stage.
// From: rocket-chip/src/main/scala/rocket/CSR.scala
type CSRDecodeIO struct {
	CSR           uint64
	FPIllegal     bool
	VectorIllegal bool
	FPCSR         bool
	RoccIllegal   bool
	ReadIllegal   bool
	WriteIllegal  bool
	WriteFlush    bool
	SystemIllegal bool
}
