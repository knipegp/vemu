package protocol

// ExtensionConfig is a bitmask for configuring which risc-v extensions to use in initialization.
type ExtensionConfig uint16

// RISC-V extension options,
// these will automatically be the correct version for 32 or 64 bit
const (
	ExtM ExtensionConfig = 1 << 0
	ExtA ExtensionConfig = 1 << 1
	ExtF ExtensionConfig = 1 << 2
	ExtD ExtensionConfig = 1 << 3
	ExtC ExtensionConfig = 1 << 4
)

type IssueUnit uint8

const (
	IUInt IssueUnit = iota
	IULSU
	IUFloat
)

type FunctionalUnit uint16

const (
	FUALU    FunctionalUnit = 1 << 0
	FUJmp    FunctionalUnit = 1 << 1
	FUMem    FunctionalUnit = 1 << 2
	FUMul    FunctionalUnit = 1 << 3
	FUDiv    FunctionalUnit = 1 << 4
	FUCSR    FunctionalUnit = 1 << 5
	FUFPU    FunctionalUnit = 1 << 6
	FUFDV    FunctionalUnit = 1 << 7
	FUI2F    FunctionalUnit = 1 << 8
	FUF2I    FunctionalUnit = 1 << 9
	FUF2IMem FunctionalUnit = FUF2I & FUMem
)

type Width uint8

const (
	Byte Width = iota
	Half
	Word
	Double
	Quad
)

type RegType uint8

const (
	RTNone RegType = iota
	RTFix
	RTFloat
	RTPass
)

type CSRCmd uint8

const (
	CSRN CSRCmd = 0
)

type MemCmd uint32

const (
	MXRD MemCmd = iota
	MXWR
	MPFR
	MPFW
	MXASWAP
	MFLUSHALL
	MXLR
	MXSC
	MXAADD
	MXAXOR
	MXAOR
	MXAAND
	MXAMIN
	MXAMAX
	MXAMINU
	MXAMAXU
	MFLUSH
	MPWR
	MPRODUCE
	MCLEAN
	MSFENCE
	MWOK
)

type UOpcode uint8

const (
	UopNOP UOpcode = iota
	UopLD
	UopSTA // store address generation
	UopSTD // store data generation
	UopLUI

	UopADDI
	UopANDI
	UopORI
	UopXORI
	UopSLTI
	UopSLTIU
	UopSLLI
	UopSRAI
	UopSRLI

	UopSLL
	UopADD
	UopSUB
	UopSLT
	UopSLTU
	UopAND
	UopOR
	UopXOR
	UopSRA
	UopSRL

	UopBEQ
	UopBNE
	UopBGE
	UopBGEU
	UopBLT
	UopBLTU
	UopCSRRW
	UopCSRRS
	UopCSRRC
	UopCSRRWI
	UopCSRRSI
	UopCSRRCI

	UopJ
	UopJAL
	UopJALR
	UopAUIPC

	//uopSRET = 40.U(UOPC_SZ.W)
	_
	UopCFLSH
	UopFENCE

	UopADDIW
	UopADDW
	UopSUBW
	UopSLLIW
	UopSLLW
	UopSRAIW
	UopSRAW
	UopSRLIW
	UopSRLW
	UopMUL
	UopMULH
	UopMULHU
	UopMULHS
	UopMULW
	UopDIV
	UopDIVU
	UopREM
	UopREMU
	UopDIVW
	UopDIVUW
	UopREMW
	UopREMUW

	UopFENCEI

	UopAMOAG // AMO-address gen (use normal STD for datagen)

	UopFMVSX
	UopFMVDX
	UopFMVXS
	UopFMVXD

	UopFSGNJS
	UopFSGNJD

	UopFCVTSD
	UopFCVTDS

	UopFCVTSX
	UopFCVTDX

	UopFCVTXS
	UopFCVTXD

	UopCMPRS
	UopCMPRD

	UopFCLASSS
	UopFCLASSD

	UopFMINMAXS
	UopFMINMAXD

	UopFADDS
	UopFSUBS
	UopFMULS
	UopFADDD
	UopFSUBD
	UopFMULD

	UopFMADDS
	UopFMSUBS
	UopFNMADDS
	UopFNMSUBS
	UopFMADDD
	UopFMSUBD
	UopFNMADDD
	UopFNMSUBD

	UopFDIVS
	UopFDIVD
	UopFSQRTS
	UopFSQRTD

	UopWFI  // pass uop down the CSR pipeline
	UopERET // pass uop down the CSR pipeline, also is ERET
	UopSFENCE

	UopROCC

	UopMOV // conditional mov decoded from "add rd, x0, rs2"
)

// Uop contains the information passed through the pipeline for every operation.
type Uop struct {
	UOpcode       UOpcode
	InsnWord      uint32
	DebugInsnWord uint32
	IsRVC         bool
	DebugPC       uint64
	Iss           IssueUnit
	Func          FunctionalUnit

	IssueWindow uint8
	IWP1Poison  bool
	IWP2Poison  bool

	IsBranch bool
	IsJalr   bool
	IsJal    bool
	IsSFB    bool

	BranchMask uint8
	BranchTag  uint8

	FTQIdx    uint8
	EdgeInsn  bool
	PCLowBits uint8

	BranchTaken bool
	ImmPacked   uint64

	CSRAddr uint16
	ROBIdx  uint8
	LDWIdx  uint8
	STQIdx  uint8
	RXQIdx  uint8

	PhyDst  uint8
	PhyRS1  uint8
	PhyRS2  uint8
	PhyRS3  uint8
	PhyPred uint8

	PhyRS1Busy  bool
	PhyRS2Busy  bool
	PhyRS3Busy  bool
	PhyPredBusy bool
	StalePhyDst uint8

	Xcpt        bool
	XcptCause   XcptCause
	Bypass      bool
	MemCmd      MemCmd
	MemSz       Width
	MemSigned   bool
	IsFence     bool
	IsFencei    bool
	IsAMO       bool
	UsesLDQ     bool
	UsesSTQ     bool
	IsSysPC2EPC bool
	IsUnique    bool

	FlushOnCommit bool

	IsSFBBranch bool
	IsSFBShadow bool
	LogDstIsRS1 bool

	LogDst uint8
	LogRS1 uint8
	LogRS2 uint8
	LogRS3 uint8

	IsDst       bool
	DstRtype    RegType
	LogRS1RType RegType
	LogRS2RType RegType
	FRS3        bool

	FPVal    bool
	FPSingle bool

	XcptPgFlt      bool
	XcptICache     bool
	XcptMisaligned bool
	BreakDebug     bool
	BreakXcpt      bool

	DebugFsrc uint8
	DebugTsrc uint8

	AllocateBranchTag bool
	RFWen             bool
	Unsafe            bool
}

type XcptCause uint16

const (
	NoCause XcptCause = iota
	DebugTriggerCause
	BreakCause
	FetchPgFaultCause
	FetchAccessCause
	IllInsnCause
)
