package protocol

// ExecRspType enumerates the type of Exec stage response.
type ExecRspType uint16

const (
	// EmptyExecRsp indicates no response.
	EmptyExecRsp ExecRspType = iota
	// JmpPCRsp indicates the response contains a new target PC.
	JmpPCRsp
	// ExecMemWrite indicates the response delivers data to the memory module.
	ExecMemWrite
	// ExecMemRead indicates the exec stage needs to read from memory.
	ExecMemRead
	// AMORsp indicates that the memory unit should execute an atomic operation.
	AMORsp
	// LRRsp indicates that the memory unit should commit a load reservation.
	LRRsp
	// SCRsp indicates that the memory unit should attempt a store conditional
	// operation.
	SCRsp
	// PrintfRsp indicates that the memory unit should read bytes and the exec
	// stage should print those bytes to stdout.
	PrintfRsp
	// ExitRsp indicates that the controller should cease execution.
	ExitRsp
)

// ExecRsp is emitted by the exec stage and inspected by other modules in the
// pipeline.
type ExecRsp struct {
	Type  ExecRspType
	Read  MemRead
	Write MemWrite
	Jmp   FetchJmp
	// AMO   AMOOp
	LR    LoadRes
	Print Printf
	Exit  Exit
}

// ExecCommit contains info for the exec stage to commit state.
type ExecCommit struct {
	InsnWord uint32
	PC       uint64
}

// Printf contains information needed to service a Printf call.
type Printf struct {
	Addr uint64
	Len  uint64
}

type Exit struct {
	Code int
}
