package protocol

// MemRspType enumerates the types of responses made by the memory module.
type MemRspType byte

const (
	// InvalidMemRsp indicates a bad response.
	InvalidMemRsp MemRspType = iota
	// EmptyMemRsp indicates no response.
	EmptyMemRsp
	// MemReadRsp indicates that the memory module is responding with data.
	MemReadRsp
)

// MemTransLen is the byte length of a single read or write transaction.
const MemTransLen uint64 = 16

// MemRsp contains fields for all memory response types.
type MemRsp struct {
	Type MemRspType
	// TODO: Get rid of dynamic structure.
	Data []byte
}

// MemRead contains info for a memory read operation.
type MemRead struct {
	Addr uint64
	Len  uint64
}

// MemWrite contains info for a memory write.
type MemWrite struct {
	Addr uint64
	Len  uint64
	Data [8]byte
}

// LoadRes contains information for a load reservation operation.
type LoadRes struct {
	Addr   uint64
	Len    uint64
	HartID uint64
}
