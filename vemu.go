package main

import (
	"flag"
	"fmt"
	"math"

	"gitlab.com/knipegp/vemu/regcomp"
	"gitlab.com/knipegp/vemu/vemulator"

	"gitlab.com/akita/mem"
	"gitlab.com/knipegp/vemu/riscv"
)

func compareExecution(base string, test string) {
	baseRegs := regcomp.Getallstates(base)
	testRegs := regcomp.Getallstates(test)
	fmt.Printf("**Register State Comparison Diff**\n")
	fmt.Printf("%s state count: %d\n", base, len(baseRegs))
	fmt.Printf("%s state count: %d\n", test, len(testRegs))
	idxmax := int(math.Min(float64(len(baseRegs)), float64(len(testRegs))))
	for idx := 0; idx < idxmax; idx++ {
		baseState := regcomp.GetUpdate(baseRegs, idx)
		testState := regcomp.GetUpdate(testRegs, idx)
		diff := regcomp.UpComp{}.WithBase(baseState).WithTest(testState).GetDiff()
		if len(diff.List) > 0 {
			fmt.Printf("Diff @ 0x%x; instruction count: %d\n %s\n", baseRegs[idx][31], idx, diff.Print())
		}
		if diff.Major {
			fmt.Printf("Major Diff\n")
			break
		}
	}
}

func main() {
	var base, test, binaryFileName string
	var ip, parse bool
	var xlen, coreCnt int
	flag.StringVar(&base, "base", "./gdb.txt", "Path to register state log file A")
	flag.StringVar(&test, "test", "./vemu.log", "Path to register state log file B")
	flag.StringVar(&binaryFileName, "executable", "./a.out", "Location of executable.")
	flag.IntVar(&xlen, "xlen", 64, "Bit length of emulator operations.")
	flag.IntVar(&coreCnt, "cores", 1, "specify the number of cores to emulate")
	flag.BoolVar(&parse, "parse", false, "Provide to compare register state logs")
	flag.BoolVar(&ip, "log", false, "provide to enable register log")
	flag.Parse()
	if parse {
		compareExecution(base, test)
	} else {
		vemu := vemulator.Builder{}.
			WithXlen(riscv.Xlen(xlen)).
			WithElf(binaryFileName).
			WithLog(ip).
			WithMemCap(4 * mem.GB).
			WithCoreCount(coreCnt).
			Build()
		exitCode, err := vemu.Run()
		if err != nil {
			panic(err)
		}
		if exitCode != 0 {
			panic(fmt.Errorf("exit code: %d", exitCode))
		}
	}
}
