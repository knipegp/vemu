# VEmu

RISC-V ISA emulator

## ISA Support

- User-level workloads
  - Basic syscalls: exit, write (to stdout only)
- RV32IC
- RV64I

## Feature Roadmap

The project goal is to support simulation fast-forwarding for a [BOOM
Core](https://docs.boom-core.org/en/latest/sections/intro-overview/boom.html)
software simulator. The final emulator will have to support at least the same
ISA features as that core.

- [ ] RV64GC
    - [ ] RV64IC
    - [ ] RV64IM
    - [ ] RV64IF
    - [ ] RV64ID
    - [ ] RV64IA
- [ ] Hardware interrupts for GPU co-simulation

## Compiling RISC-V Binaries

RISC-V cross compilation requires an installation of the [riscv-gnu-toolchain](
https://github.com/riscv/riscv-gnu-toolchain). Some Linux package managers carry
these tools by default. Our [Testing Docker
container](registry.gitlab.com/knipegp/vemu/golang-riscv-gcc:0.1.0) also
supports compilation.

### Compile RISC-V ISA Tests and Benchmarks

```bash
cd test/
make
```

### Compiling RV32I Custom Binaries

Run the following command with the paths to target source to compile binaries
for the emualtor. Set the `-march` and `-mabi` flags for the desired ISA
extensions. For example, compile an RV32I binary with flags, `-march=rv32i`
`-mabi=ilp32`.

```bash
riscv64-unknown-elf-gcc \
-march=rv64i \
-mabi=lp64 -static \
-mcmodel=medany \
-fvisibility=hidden \
-nostdlib \
-nostartfiles \
-ffreestanding \
-I./src \
-I./riscv-tests/env \
-T./riscv-tests/env/p/link.ld \
src/syscalls.c \
src/crt.S \
<src files>
```

## Emulate and Debug with Spike

**The testing scripts in this repository are old and may not work.**

Use `test/run_debug.sh`.

### GDB Init

Add the following text to `~/.gdbinit` to create a qemu-gdb log.

```
set pagination off
set logging file gdb.txt
set logging overwrite on
set logging redirect on
set logging on
set style enabled off
define logrun
    while 1 
    i r
    si
    end
end
```

To capture all register states up to running main, enter the following into GDB
manually. GDB resources read that this should be able to run in a GDB init
script, but I was not able to configure the RISC-V GDB to do so.

```
while $pc != 0x10480
i r
si
end
```
