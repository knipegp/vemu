package memory

import (
	"math"

	"gitlab.com/knipegp/vemu/bits"
	"gitlab.com/knipegp/vemu/riscv"
)

const (
	setCnt  uint64 = 64
	setWays uint64 = 4
	// RowBytes describes the byte length of a cache row.
	RowBytes uint64 = 16
	// RowAlignMask is used to align an address to a row boundary.
	RowAlignMask uint64 = ^(RowBytes - 1)
)

type cacheSet struct {
	rows [setWays][RowBytes]byte
	lru  [setWays]uint64
	tags [setWays]uint64
}

func newCacheSet() cacheSet {
	var l [setWays]uint64
	for idx := uint64(0); idx < setWays; idx++ {
		l[idx] = idx
	}
	c := cacheSet{lru: l}
	return c
}

func (c *cacheSet) lruAccess(setIdx uint64) {
	var i, idx uint64
	for idx = 0; idx < setWays; idx++ {
		if c.lru[idx] == setIdx {
			i = idx
			break
		}
	}
	if idx == setWays {
		panic("invalid setIdx")
	}
	tmp := c.lru[i]
	for ; i > 0; i-- {
		c.lru[i] = c.lru[i-1]
	}
	c.lru[0] = tmp
}

func (c *cacheSet) readRow(tag uint64) (row [RowBytes]byte, ok bool) {
	for idx := uint64(0); idx < setWays; idx++ {
		if c.tags[idx] == tag {
			row = c.rows[idx]
			c.lruAccess(idx)
			ok = true
			break
		}
	}
	return
}

func (c *cacheSet) populateRow(tag uint64, data [RowBytes]byte) {
	lruRowIdx := c.lru[setWays-1]
	c.lruAccess(lruRowIdx)
	c.tags[lruRowIdx] = tag
	c.rows[lruRowIdx] = data
}

// Cache32 is a set-associative cache that can read in 32-bit chunks.
//
// This implementation is not intended to be written to by the CPU core. The use
// of dynamic structures is restricted for performance.
type Cache32 struct {
	sets   [setCnt]cacheSet
	xlen   riscv.Xlen
	offMSB int
	idxLSB int
	idxMSB int
	tagLSB int
	tagMSB int
}

// NewCache32 creates a new instance of Cache32
func NewCache32(xlen riscv.Xlen) Cache32 {
	offMSB := int(math.Log2(float64(RowBytes))) - 1
	idxLSB := offMSB + 1
	idxMSB := idxLSB + int(math.Log2(float64(setCnt))) - 1
	tagLSB := idxMSB + 1
	tagMSB := int(xlen) - 1
	ret := Cache32{xlen: xlen, offMSB: offMSB, idxLSB: idxLSB, idxMSB: idxMSB, tagLSB: tagLSB, tagMSB: tagMSB}
	for idx := uint64(0); idx < setCnt; idx++ {
		ret.sets[idx] = newCacheSet()
	}
	return ret
}

// Read32 reads retrieves a uint32 from the cache.
func (b *Cache32) Read32(addr uint64) (data uint32, ok bool) {
	if b.xlen == riscv.Bit32 && !bits.Is32BitSignExt(addr) {
		panic("32-bit address must be sign extended")
	}
	tag := bits.GetBits(addr, b.tagMSB, b.tagLSB)
	setIdx := bits.GetBits(addr, b.idxMSB, b.idxLSB)
	off := bits.GetBits(addr, b.offMSB, 0)
	// 4 = readlen
	if off > RowBytes-4 {
		panic("requested address and read length will read over multiple cache rows")
	}
	row, ok := b.sets[setIdx].readRow(tag)
	if ok {
		for idx := uint64(0); idx < 4; idx++ {
			data |= uint32(row[idx+off]) << (idx * 8)
		}
	}
	return
}

// Read16 reads retrieves a uint16 from the cache.
func (b *Cache32) Read16(addr uint64) (data uint16, ok bool) {
	if b.xlen == riscv.Bit32 && !bits.Is32BitSignExt(addr) {
		panic("32-bit address must be sign extended")
	}
	tag := bits.GetBits(addr, b.tagMSB, b.tagLSB)
	setIdx := bits.GetBits(addr, b.idxMSB, b.idxLSB)
	off := bits.GetBits(addr, b.offMSB, 0)
	// 2 = readlen
	if off > RowBytes-2 {
		panic("requested address and read length will read over multiple cache rows")
	}
	row, ok := b.sets[setIdx].readRow(tag)
	if ok {
		for idx := uint64(0); idx < 2; idx++ {
			data |= uint16(row[idx+off]) << (idx * 8)
		}
	}
	return
}

// Populate adds a new line to the cache
func (b *Cache32) Populate(addr uint64, data [RowBytes]byte) {
	if b.xlen == riscv.Bit32 && !bits.Is32BitSignExt(addr) {
		panic("32-bit address must be sign extended")
	}
	if addr&RowAlignMask != addr {
		panic("cache32 populate misaligned address")
	}
	tag := bits.GetBits(addr, b.tagMSB, b.tagLSB)
	idx := bits.GetBits(addr, b.idxMSB, b.idxLSB)
	b.sets[idx].populateRow(tag, data)
}
