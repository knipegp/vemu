package memory

import (
	"testing"

	"gitlab.com/knipegp/vemu/riscv"
)

func BenchmarkRuntime(b *testing.B) {
	cache := NewCache32(riscv.Bit32)
	var reqs uint64 = 100000000
	for idx := uint64(0); idx < reqs; idx++ {
		var offset uint64 = (idx * 4) % 16384
		var addr uint64 = 0xffffffff80000000 + offset
		_, ok := cache.Read32(addr)
		if !ok {
			var data [RowBytes]byte
			for dataIdx := uint64(0); dataIdx < RowBytes; dataIdx++ {
				data[dataIdx] = byte(dataIdx)
			}
			cache.Populate(addr&RowAlignMask, data)
			_, ok = cache.Read32(addr)
			if !ok {
				panic("did not populate correctly")
			}
		}
	}
}
