package memory

import (
	"encoding/binary"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/knipegp/vemu/riscv"
)

var _ = Describe("Icache", func() {
	var (
		cache       Cache32
		sameSetAddr [setWays + 1]uint64
		sampleRows  [setWays + 1][RowBytes]byte
	)
	Context("Xlen 32", func() {
		BeforeEach(func() {
			cache = NewCache32(riscv.Bit32)
			for wayIdx := uint64(0); wayIdx < setWays+1; wayIdx++ {
				sameSetAddr[wayIdx] = 0xffffffff80000000 + ((uint64(64) << cache.idxLSB) * wayIdx)
				for rowIdx := uint64(0); rowIdx < RowBytes; rowIdx++ {
					sampleRows[wayIdx][rowIdx] = byte(rowIdx + (RowBytes * wayIdx))
				}
			}
			for idx := uint64(0); idx < setWays; idx++ {
				cache.Populate(sameSetAddr[idx], sampleRows[idx])
			}
		})
		It("should miss", func() {
			_, ok := cache.Read32(sameSetAddr[setWays])
			Expect(ok).To(Equal(false))
		})
		It("should hit the same set", func() {
			for idx := uint64(0); idx < setWays; idx++ {
				setAddr := sameSetAddr[idx]
				checkRow := sampleRows[idx]
				for off := uint64(0); off < RowBytes; off += 4 {
					data, ok := cache.Read32(setAddr + off)
					Expect(ok).To(Equal(true))
					check := binary.LittleEndian.Uint32(checkRow[off : off+4])
					Expect(data).To(Equal(check))
				}
			}
		})
		It("should evict the first row", func() {
			evictedAddr := sameSetAddr[0]
			newAddr := sameSetAddr[setWays]
			newRow := sampleRows[setWays]
			cache.Populate(newAddr, newRow)
			_, ok := cache.Read32(evictedAddr)
			Expect(ok).To(Equal(false))
			for off := uint64(0); off < RowBytes; off += 4 {
				data, ok := cache.Read32(newAddr + off)
				Expect(ok).To(Equal(true))
				check := binary.LittleEndian.Uint32(newRow[off : off+4])
				Expect(data).To(Equal(check))
			}
		})
		It("should evict the second row", func() {
			firstAddr := sameSetAddr[0]
			secondAddr := sameSetAddr[1]
			newAddr := sameSetAddr[setWays]
			newRow := sampleRows[setWays]
			cache.Read32(firstAddr)
			cache.Populate(newAddr, newRow)
			_, ok := cache.Read32(secondAddr)
			Expect(ok).To(Equal(false))
			for off := uint64(0); off < RowBytes; off += 4 {
				data, ok := cache.Read32(newAddr + off)
				Expect(ok).To(Equal(true))
				check := binary.LittleEndian.Uint32(newRow[off : off+4])
				Expect(data).To(Equal(check))
			}
		})
	})
})
