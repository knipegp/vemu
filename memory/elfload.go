package memory

import (
	"debug/elf"
	"encoding/binary"
	"fmt"
	"path/filepath"

	"gitlab.com/knipegp/vemu/riscv"
)

// A Loadable is a memory structure to which data can be written.
type Loadable interface {
	Write(address uint64, data []byte) error
}

// Loader loads a program into memory. Each implementation makes different
// assumptions regarding the layout of the elf in memory.
type Loader interface {
	Load(store Loadable)
	Entry() uint64
	// TODO: This should not be part of this interface. The HTIF (and similar
	// functionality) should be broken out into a separate module.
	ToHostAddr() uint64
}

// GetElf retrieves the elf file object for the filepath.
func GetElf(binaryPath string) *elf.File {
	binaryPath, err := filepath.Abs(binaryPath)
	if err != nil {
		panic(err)
	}
	file, err := elf.Open(binaryPath)
	if err != nil {
		panic(err)
	}
	return file
}

func loadProg(storage Loadable, prog *elf.Prog) {
	slicelen := prog.ProgHeader.Filesz
	if slicelen <= 0 {
		return
	}
	memaddr := prog.Paddr
	progbin := make([]byte, slicelen)
	readlen, err := prog.ReadAt(progbin, 0)
	if err != nil {
		panic(err)
	}
	if uint64(readlen) != slicelen {
		panic(fmt.Errorf("too few bytes read"))
	}
	storage.Write(memaddr, progbin)
}

// LoadPrograms writes the LOAD prog headers to the provided storage.
func LoadPrograms(storage Loadable, elfFile *elf.File) {
	var loadables []*elf.Prog
	for _, prog := range elfFile.Progs {
		if prog.Type == elf.PT_LOAD {
			loadables = append(loadables, prog)
		}
	}
	for _, elfProg := range loadables {
		loadProg(storage, elfProg)
	}
}

// SpikeLoad loads a program into memory.
//
// This implementation assumes that the elf was compiled to run on the Spike ISA
// simulator.
type SpikeLoad struct {
	*elf.File
	xlen riscv.Xlen
	// The entry point is manually modified from the value in the elf.
	entry uint64
}

// NewSpikeLoad initializes a new loader for the given elf path.
func NewSpikeLoad(elfPath string, xlen riscv.Xlen) *SpikeLoad {
	newFile := GetElf(elfPath)
	// TODO: The tohost module should be moved into a board support module.
	class := newFile.Class
	switch xlen {
	case riscv.Bit32:
		if class != elf.ELFCLASS32 {
			panic(fmt.Errorf("requested xlen, %v, does not match elf, %v", xlen, class))
		}
	case riscv.Bit64:
		if class != elf.ELFCLASS64 {
			panic(fmt.Errorf("requested xlen, %v, does not match elf, %v", xlen, class))
		}
	default:
		panic(fmt.Errorf("unknown xlen, %v", xlen))
	}
	return &SpikeLoad{newFile, xlen, 0x1000}
}

// Load loads a baremetal program into the passed storage.
func (s SpikeLoad) Load(store Loadable) {
	// Load DTB instructions address
	// https://github.com/riscv/riscv-isa-sim/issues/145
	// Little endian instructions
	var crt []byte
	if s.xlen == riscv.Bit64 {
		crt = []byte{
			// auipc   t0, 0x0
			0x97, 0x2, 0, 0,
			// addi    a1, t0, 32
			0x93, 0x85, 0x2, 0x2,
			// csrr    a0, mhartid
			0x73, 0x25, 0x40, 0xf1,
			// ld      t0, 24(t0)
			0x83, 0xb2, 0x82, 0x1,
			// jr      t0
			0x67, 0x80, 0x2, 0,
		}
	} else {
		crt = []byte{
			// auipc   t0, 0x0
			0x97, 0x2, 0, 0,
			// addi    a1, t0, 32
			0x93, 0x85, 0x2, 0x2,
			// csrr    a0, mhartid
			0x73, 0x25, 0x40, 0xf1,
			// lw      t0, 24(t0)
			0x83, 0xa2, 0x82, 0x1,
			// jr      t0
			0x67, 0x80, 0x2, 0,
		}
	}
	store.Write(s.entry, crt)
	entryPc := make([]byte, 4)
	binary.LittleEndian.PutUint32(entryPc, uint32(s.File.Entry))
	store.Write(s.entry+24, entryPc)
	LoadPrograms(store, s.File)
}

// Entry retrieves the elf entry point.
func (s SpikeLoad) Entry() uint64 {
	return s.entry
}

// ToHostAddr retrieves the tohost interface memory address.
func (s SpikeLoad) ToHostAddr() uint64 {
	secPtr := s.Section(".tohost")
	if secPtr != nil {
		return secPtr.Addr
	}
	return 0
}
