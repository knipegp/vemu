package memory

import (
	"math"

	"gitlab.com/akita/mem"
	"gitlab.com/knipegp/vemu/bits"
	"gitlab.com/knipegp/vemu/riscv"
)

// Mem is the most basic interface to a memory unit.
type Mem interface {
	Read(addr uint64, len uint64) ([]byte, error)
	Write(addr uint64, data []byte) error
}

// BasicMem is a memory unit that also contains an HTIF.
type BasicMem struct {
	core *mem.Storage
	xlen riscv.Xlen
}

// NewBasicMem returns a new instance of HTIFMem, containing a MemWrapper core
// and barrier used to trigger HTIF requests.
func NewBasicMem(cap uint64, xlen riscv.Xlen) *BasicMem {
	return &BasicMem{core: mem.NewStorage(cap), xlen: xlen}
}

// Read retrieves data from the memory unit.
func (h *BasicMem) Read(addr uint64, len uint64) ([]byte, error) {
	var coreAddr uint64
	if h.xlen == riscv.Bit32 {
		if !bits.Is32BitSignExt(addr) {
			panic("32-bit address must be sign extended")
		}
		coreAddr = addr & math.MaxUint32
	} else {
		coreAddr = addr
	}
	return h.core.Read(coreAddr, len)
}

// Write updates data in the unit and checks if the HTIF was written.
func (h *BasicMem) Write(addr uint64, data []byte) error {
	var coreAddr uint64
	if h.xlen == riscv.Bit32 {
		if !bits.Is32BitSignExt(addr) {
			panic("32-bit address must be sign extended")
		}
		coreAddr = addr & math.MaxUint32
	} else {
		coreAddr = addr
	}
	err := h.core.Write(coreAddr, data)
	return err
}
