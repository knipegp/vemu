package regfile

// CSR Address listing
const (
	USTATUS = 0x0
	UIE     = 0x4
	UTVEC   = 0x5

	UESCRATCH = 0x40
	UEPC      = 0x41
	UCAUSE    = 0x42
	UTVAL     = 0x43
	UIP       = 0x44

	FFLAGS = 0x1
	FRMID  = 0x2
	FCSR   = 0x3

	// supervisor CSRs

	SSTATUS     = 0x100
	SEDELEG     = 0x102
	SIDELEG     = 0x103
	SIE         = 0x104
	STVEC       = 0x105
	SCOUNTERLEN = 0x106

	SSCRATCH = 0x140
	SEPC     = 0x141
	SCAUSE   = 0x142
	STVAL    = 0x143
	SIP      = 0x144

	// Machine IDs

	MVENDORID = 0xf11
	MARCHID   = 0xf12
	MIMPID    = 0xf13
	MHARTID   = 0xf14

	MISA       = 0x301
	MEDELEG    = 0x302
	MIDELEG    = 0x303
	MIE        = 0x304
	MTVEC      = 0x305
	MCOUNTEREN = 0x306

	MSTATUS = 0x340
	MEPC    = 0x341
	MCAUSE  = 0x342
	MTVAL   = 0x343
	MIP     = 0x344
)

type csr interface {
	write(val uint64)
	read() uint64
}

type defaultCSR struct {
	val uint64
}

func (d *defaultCSR) write(val uint64) {
	d.val = val
}

func (d *defaultCSR) read() uint64 {
	return d.val
}

type customCsr struct {
	val      *uint64
	bitmask  uint64
	shiftCnt int
}

func newCustomCsr(val *uint64, bitmask uint64) *customCsr {
	shiftCnt := 0
	for tmp := bitmask; tmp&0x1 == 0; tmp /= 2 {
		shiftCnt++
	}
	return &customCsr{val, bitmask, shiftCnt}
}

func (c *customCsr) write(val uint64) {
	*c.val &= ^c.bitmask
	*c.val |= (val << c.shiftCnt) & c.bitmask
}

func (c *customCsr) read() uint64 {
	return (c.bitmask & *c.val) >> c.shiftCnt
}

// CsrRegfile contains all control status registers for a core.
type CsrRegfile struct {
	regs [4096]csr
}

// NewCsrRegfile returns a new instance of a CSR register file.
func NewCsrRegfile(coreID uint64) *CsrRegfile {
	csrs := &CsrRegfile{}
	for idx := 0; idx < 4096; idx++ {
		csrs.regs[idx] = &defaultCSR{0}
	}
	fcsrVal := new(uint64)
	newFcsr := newCustomCsr(fcsrVal, 0xff)
	newFflags := newCustomCsr(fcsrVal, 0x1f)
	newFrm := newCustomCsr(fcsrVal, 0xe0)
	csrs.regs[FCSR] = newFcsr
	csrs.regs[FFLAGS] = newFflags
	csrs.regs[FRMID] = newFrm
	csrs.Write(MHARTID, coreID)
	return csrs
}

// Read returns the CSR value for the given CSR index.
func (c *CsrRegfile) Read(index int) uint64 {
	return c.regs[index].read()
}

// Write applies the value to the CSR at the given index.
func (c *CsrRegfile) Write(index int, val uint64) {
	c.regs[index].write(val)
}
