package regfile

import (
	"fmt"
	"math"

	"gitlab.com/knipegp/vemu/riscv"
)

const (
	logFormat string = "ra  0x%x\n" +
		"sp  %#x\n" +
		"gp  %#x\n" +
		"tp  %#x\n" +
		"t0  %#x\n" +
		"t1  %#x\n" +
		"t2  %#x\n" +
		"fp  %#x\n" +
		"s1  %#x\n" +
		"a0  %#x\n" +
		"a1  %#x\n" +
		"a2  %#x\n" +
		"a3  %#x\n" +
		"a4  %#x\n" +
		"a5  %#x\n" +
		"a6  %#x\n" +
		"a7  %#x\n" +
		"s2  %#x\n" +
		"s3  %#x\n" +
		"s4  %#x\n" +
		"s5  %#x\n" +
		"s6  %#x\n" +
		"s7  %#x\n" +
		"s8  %#x\n" +
		"s9  %#x\n" +
		"s10 %#x\n" +
		"s11 %#x\n" +
		"t3  %#x\n" +
		"t4  %#x\n" +
		"t5  %#x\n" +
		"t6  %#x\n"
)

// RegFile provides methods for reading and writing a register file.
type RegFile interface {
	Read(reg RegName) uint64
	Write(reg RegName, val uint64)
	String() string
}

// IntRegFile32 is an integer register file compatible with Xlen 32.
type IntRegFile32 struct {
	registers [32]uint32
}

// Read retrieves the corresponding value to the desired register.
func (i *IntRegFile32) Read(reg RegName) uint64 {
	regIdx := int(reg)
	return uint64(int32(i.registers[regIdx]))
}

// Write sets the register to the passed value.
func (i *IntRegFile32) Write(reg RegName, val uint64) {
	regIdx := int(reg)
	if reg != Zero {
		i.registers[regIdx] = uint32(val)
	}
}

// String returns the current regfile state as a string.
func (i IntRegFile32) String() string {
	log := fmt.Sprintf(logFormat,
		i.registers[Ra]&math.MaxUint32,
		i.registers[Sp]&math.MaxUint32,
		i.registers[Gp]&math.MaxUint32,
		i.registers[Tp]&math.MaxUint32,
		i.registers[T0]&math.MaxUint32,
		i.registers[T1]&math.MaxUint32,
		i.registers[T2]&math.MaxUint32,
		i.registers[Fp]&math.MaxUint32,
		i.registers[S1]&math.MaxUint32,
		i.registers[A0]&math.MaxUint32,
		i.registers[A1]&math.MaxUint32,
		i.registers[A2]&math.MaxUint32,
		i.registers[A3]&math.MaxUint32,
		i.registers[A4]&math.MaxUint32,
		i.registers[A5]&math.MaxUint32,
		i.registers[A6]&math.MaxUint32,
		i.registers[A7]&math.MaxUint32,
		i.registers[S2]&math.MaxUint32,
		i.registers[S3]&math.MaxUint32,
		i.registers[S4]&math.MaxUint32,
		i.registers[S5]&math.MaxUint32,
		i.registers[S6]&math.MaxUint32,
		i.registers[S7]&math.MaxUint32,
		i.registers[S8]&math.MaxUint32,
		i.registers[S9]&math.MaxUint32,
		i.registers[S10]&math.MaxUint32,
		i.registers[S11]&math.MaxUint32,
		i.registers[T3]&math.MaxUint32,
		i.registers[T4]&math.MaxUint32,
		i.registers[T5]&math.MaxUint32,
		i.registers[T6]&math.MaxUint32,
	)
	return log
}

// IntRegFile64 is an integer register file compatible with Xlen 64.
type IntRegFile64 struct {
	registers [32]uint64
}

// Read retrieves the corresponding value to the desired register.
func (i *IntRegFile64) Read(reg RegName) uint64 {
	return i.registers[int(reg)]
}

// Write sets the register to the passed value.
func (i *IntRegFile64) Write(reg RegName, val uint64) {
	if reg != Zero {
		i.registers[int(reg)] = val
	}
}

// String returns the current regfile state as a string.
func (i IntRegFile64) String() string {
	log := fmt.Sprintf(logFormat,
		i.registers[Ra],
		i.registers[Sp],
		i.registers[Gp],
		i.registers[Tp],
		i.registers[T0],
		i.registers[T1],
		i.registers[T2],
		i.registers[Fp],
		i.registers[S1],
		i.registers[A0],
		i.registers[A1],
		i.registers[A2],
		i.registers[A3],
		i.registers[A4],
		i.registers[A5],
		i.registers[A6],
		i.registers[A7],
		i.registers[S2],
		i.registers[S3],
		i.registers[S4],
		i.registers[S5],
		i.registers[S6],
		i.registers[S7],
		i.registers[S8],
		i.registers[S9],
		i.registers[S10],
		i.registers[S11],
		i.registers[T3],
		i.registers[T4],
		i.registers[T5],
		i.registers[T6],
	)
	return log
}

// NewIntRegFile initializes the register file.
func NewIntRegFile(xlen riscv.Xlen) RegFile {
	switch xlen {
	case riscv.Bit32:
		return &IntRegFile32{}
	case riscv.Bit64:
		return &IntRegFile64{}
	default:
		panic(fmt.Errorf("unknown xlen, %v", xlen))
	}
}
