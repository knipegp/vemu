package regfile_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestRegfile(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Regfile Suite")
}
