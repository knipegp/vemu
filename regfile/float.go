package regfile

import "math"

// FloatRegName is an index into the floating point register file.
type FloatRegName int

const (
	// Ft0 is a floating point temporary register.
	Ft0 FloatRegName = iota
	// Ft1 is a floating point temporary register.
	Ft1
	// Ft2 is a floating point temporary register.
	Ft2
	// Ft3 is a floating point temporary register.
	Ft3
	// Ft4 is a floating point temporary register.
	Ft4
	// Ft5 is a floating point temporary register.
	Ft5
	// Ft6 is a floating point temporary register.
	Ft6
	// Ft7 is a floating point temporary register.
	Ft7
	// Fs0 is a floating point saved register.
	Fs0
	// Fs1 is a floating point saved register.
	Fs1
	// Fa0 is a floating point argument register.
	Fa0
	// Fa1 is a floating point argument register.
	Fa1
	// Fa2 is a floating point argument register.
	Fa2
	// Fa3 is a floating point argument register.
	Fa3
	// Fa4 is a floating point argument register.
	Fa4
	// Fa5 is a floating point argument register.
	Fa5
	// Fa6 is a floating point argument register.
	Fa6
	// Fa7 is a floating point argument register.
	Fa7
	// Fs2 is a floating point saved register.
	Fs2
	// Fs3 is a floating point saved register.
	Fs3
	// Fs4 is a floating point saved register.
	Fs4
	// Fs5 is a floating point saved register.
	Fs5
	// Fs6 is a floating point saved register.
	Fs6
	// Fs7 is a floating point saved register.
	Fs7
	// Fs8 is a floating point saved register.
	Fs8
	// Fs9 is a floating point saved register.
	Fs9
	// Fs10 is a floating point saved register.
	Fs10
	// Fs11 is a floating point saved register.
	Fs11
	// Ft8 is a floating point temporary register.
	Ft8
	// Ft9 is a floating point temporary register.
	Ft9
	// Ft10 is a floating point temporary register.
	Ft10
	// Ft11 is a floating point temporary register.
	Ft11
)

// FloatRegFile is a register file for floating point and double precision
// values.
type FloatRegFile struct {
	regs [32]uint64
}

// ReadS retrieves a float32 value from the register file.
func (f *FloatRegFile) ReadS(reg FloatRegName) float32 {
	return math.Float32frombits(uint32(f.regs[int(reg)]))
}

// ReadD retrieves a float64 value from the register file.
func (f *FloatRegFile) ReadD(reg FloatRegName) float64 {
	return math.Float64frombits(f.regs[int(reg)])
}

// WriteS writes a float32 to the register file.
func (f *FloatRegFile) WriteS(reg FloatRegName, val float32) {
	f.regs[int(reg)] = uint64(math.Float32bits(val))
}

// WriteD writes a float32 to the register file.
func (f *FloatRegFile) WriteD(reg FloatRegName, val float64) {
	f.regs[int(reg)] = math.Float64bits(val)
}
