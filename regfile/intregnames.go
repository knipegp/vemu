package regfile

// RegName maps a register name to an index in a register file.
type RegName int

const (
	// Zero register is always 0.
	Zero RegName = iota
	// Ra register is the return address.
	Ra
	// Sp register is the stack pointer.
	Sp
	// Gp register is the global pointer.
	Gp
	// Tp register is the thread pointer.
	Tp
	// T0 register is a temporary register.
	T0
	// T1 register is a temporary register.
	T1
	// T2 register is a temporary register.
	T2
	// Fp register is the frame pointer.
	Fp
	// S1 register is a saved register.
	S1
	// A0 register can be used for function arguments and return values.
	A0
	// A1 register can be used for function arguments and return values.
	A1
	// A2 register can be used for function arguments.
	A2
	// A3 register can be used for function arguments.
	A3
	// A4 register can be used for function arguments.
	A4
	// A5 register can be used for function arguments.
	A5
	// A6 register can be used for function arguments.
	A6
	// A7 register can be used for function arguments.
	A7
	// S2 register is a saved register.
	S2
	// S3 register is a saved register.
	S3
	// S4 register is a saved register.
	S4
	// S5 register is a saved register.
	S5
	// S6 register is a saved register.
	S6
	// S7 register is a saved register.
	S7
	// S8 register is a saved register.
	S8
	// S9 register is a saved register.
	S9
	// S10 register is a saved register.
	S10
	// S11 register is a saved register.
	S11
	// T3 register is a temporary register.
	T3
	// T4 register is a temporary register.
	T4
	// T5 register is a temporary register.
	T5
	// T6 register is a temporary register.
	T6
)
