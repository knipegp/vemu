package regfile_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/knipegp/vemu/regfile"
	"gitlab.com/knipegp/vemu/riscv"
)

var _ = Describe("Intregfile", func() {
	var (
		base32File RegFile
	)
	BeforeEach(func() {
		base32File = NewIntRegFile(riscv.Bit32)
		base32File.Write(A0, 0x123)
		base32File.Write(T0, 0x456)
	})
	It("should read a0", func() {
		a0 := base32File.Read(A0)
		Expect(a0).To(Equal(uint64(0x123)))
	})
	It("should use the interface", func() {
		var iface RegFile = base32File
		t0 := iface.Read(T0)
		Expect(t0).To(Equal(uint64(0x456)))
	})
})
