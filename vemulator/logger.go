package vemulator

import (
	"log"
	"os"
	"strings"
)

// Logable is a struct that prints its current state as a string.
type Logable interface {
	String() string
}

type baseLog struct {
	*log.Logger
	keyOrder []Logable
}

func newLogger(logPath string) *baseLog {
	fileOut, err := os.Create(logPath)
	if err != nil {
		panic(err)
	}
	logger := log.New(fileOut, "", 0)
	return &baseLog{logger, make([]Logable, 0)}
}

func (b *baseLog) pushLogable(module Logable) {
	b.keyOrder = append(b.keyOrder, module)
}

// Log prints the states of all logables to the Logger object.
func (b *baseLog) Log() {
	var build strings.Builder
	for _, module := range b.keyOrder {
		build.WriteString(module.String())
	}
	b.Print(build.String())
}
