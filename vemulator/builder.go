package vemulator

import (
	"gitlab.com/knipegp/vemu/memory"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/riscv"
)

// Builder can be used to set emulation parameters beyond their
// defaults.
type Builder struct {
	elf      string
	capacity uint64
	log      bool
	xlen     riscv.Xlen
	coreCnt  int
}

// WithCoreCount defines the numbers of cores to emulate.
func (b Builder) WithCoreCount(cnt int) Builder {
	b.coreCnt = cnt
	return b
}

// WithElf defines the path to the file to be emulated.
func (b Builder) WithElf(path string) Builder {
	b.elf = path
	return b
}

// WithMemCap defines the memory capacity.
func (b Builder) WithMemCap(capacity uint64) Builder {
	b.capacity = capacity
	return b
}

// WithLog decides whether the emulator will generate an register value log.
func (b Builder) WithLog(log bool) Builder {
	b.log = log
	return b
}

// WithXlen specifies the Xlen of the simulation.
func (b Builder) WithXlen(xlen riscv.Xlen) Builder {
	b.xlen = xlen
	return b
}

// Build generates a Vemulator using the builder's values.
func (b Builder) Build() *Vemulator {
	loader := memory.NewSpikeLoad(b.elf, b.xlen)
	mem := memory.NewBasicMem(b.capacity, b.xlen)
	loader.Load(mem)
	cores := NewBasicCores(b.coreCnt, b.xlen, loader.Entry(), mem)
	var logger *baseLog
	if b.log {
		logger = newLogger("./vemu.log")
		logger.pushLogable(cores[0])
	}
	return &Vemulator{cores: cores, Logger: logger, mem: mem, rspChan: make(chan protocol.ExecRsp)}
}
