package vemulator

import (
	"fmt"

	"gitlab.com/knipegp/vemu/exec"
	"gitlab.com/knipegp/vemu/fetch"
	"gitlab.com/knipegp/vemu/memory"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/riscv"
)

// BasicCore executes instructions from memory and updates its own state.
type BasicCore struct {
	execStage  *exec.SpikeExec
	fetchStage fetch.BasicFetch
	execRspReg protocol.ExecRsp
	mem        memory.Mem
}

// NewBasicCores returns a list of cores representing execution of the same
// processor.
func NewBasicCores(coreCnt int, xlen riscv.Xlen, initPC uint64, mem memory.Mem) []*BasicCore {
	cores := make([]*BasicCore, coreCnt)
	for idx := 0; idx < coreCnt; idx++ {
		var execStage *exec.SpikeExec
		var err error
		if xlen == riscv.Bit64 {
			execStage, err = exec.NewSpikeExec(xlen, protocol.ExtM, uint64(idx))
		} else {
			// execStage, err = exec.NewSpikeExec(b.xlen, exec.ExtI|exec.ExtM|exec.ExtA|exec.ExtC|exec.ExtF)
			execStage, err = exec.NewSpikeExec(xlen, protocol.ExtC, uint64(idx))
		}
		if err != nil {
			panic(err)
		}
		fetchStage := fetch.NewBasicFetch(initPC, xlen)
		cores[idx] = &BasicCore{
			execStage:  execStage,
			fetchStage: fetchStage,
			mem:        mem,
			execRspReg: protocol.ExecRsp{Type: protocol.EmptyExecRsp},
		}
		exec.SetupMagicHandler(cores[idx].execStage)
	}
	return cores
}

func sliceToArray(data []byte) [protocol.MemTransLen]byte {
	var newArr [protocol.MemTransLen]byte
	for idx := uint64(0); idx < protocol.MemTransLen; idx++ {
		newArr[idx] = data[idx]
	}
	return newArr
}

// Tick increments core state by one cycle.
func (b *BasicCore) Tick() (execRsp protocol.ExecRsp, err error) { //nolint:gocyclo
	var fetchRsp protocol.FetchRsp
	switch b.execRspReg.Type {
	case protocol.EmptyExecRsp:
		fetchRsp = b.fetchStage.GetBitsNPC()
	case protocol.JmpPCRsp:
		fetchRsp = b.fetchStage.GetBitsJmp(b.execRspReg.Jmp.PC)
	default:
		panic("bad exec rsp")
	}
	for ; fetchRsp.Type == protocol.FetchMemRead; fetchRsp = b.fetchStage.GetBitsNPC() {
		var data []byte
		data, err = b.mem.Read(fetchRsp.Read.Addr, fetchRsp.Read.Len)
		if err != nil {
			panic(err)
		}
		arr := sliceToArray(data)
		b.fetchStage.Populate(fetchRsp.Read.Addr, arr)
	}
	execRsp, err = b.execStage.Commit(fetchRsp.Commit.InsnWord, fetchRsp.Commit.PC)
	if err == nil {
		switch execRsp.Type {
		case protocol.EmptyExecRsp, protocol.JmpPCRsp:
			b.execRspReg = execRsp
		case protocol.ExecMemRead:
			var data []byte
			data, err = b.mem.Read(execRsp.Read.Addr, execRsp.Read.Len)
			if err != nil {
				panic(err)
			}
			arr := exec.SliceToRegArr(data)
			b.execStage.WriteBack(arr, execRsp.Read.Len)
			b.execRspReg = protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		case protocol.ExecMemWrite:
			slice := execRsp.Write.Data[:execRsp.Write.Len]
			b.mem.Write(execRsp.Write.Addr, slice)
			b.execRspReg = protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		case protocol.PrintfRsp, protocol.ExitRsp:
			b.execRspReg = protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		default:
			panic("bad exec rsp")
		}
	}
	return execRsp, err
}

// String returns the current state of the module.
func (b *BasicCore) String() string {
	return fmt.Sprintf("%s\n%s\n\n", b.execStage.String(), b.fetchStage.String())
}
