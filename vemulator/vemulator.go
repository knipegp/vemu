package vemulator

import (
	"fmt"
	"sync"

	"gitlab.com/knipegp/vemu/memory"
	"gitlab.com/knipegp/vemu/protocol"
)

// Vemulator is the parent class containing the architectural state described
// by the RISC-V spec.
type Vemulator struct {
	Logger   *baseLog
	mem      *memory.BasicMem
	cores    []*BasicCore
	retVal   int
	exited   bool
	emuError error
	errLock  sync.Mutex
	rspChan  chan protocol.ExecRsp
}

func (v *Vemulator) handleInterrupt() {
	for {
		execRsp, ok := <-v.rspChan
		if !ok {
			return
		}
		switch execRsp.Type {
		case protocol.PrintfRsp:
			data, err := v.mem.Read(execRsp.Print.Addr, execRsp.Print.Len)
			if err != nil {
				panic(err)
			}
			fmt.Print(string(data))
		case protocol.ExitRsp:
			v.retVal = execRsp.Exit.Code
			return
		default:
			panic(fmt.Errorf("unexpected interrupt type: %v", execRsp))
		}
	}
}

// Run starts the emulator.
func (v *Vemulator) Run() (rv int, err error) {
	for idx, coreProc := range v.cores {
		// TODO: Support logging for multiple cores.
		var log *baseLog
		if idx == 0 {
			log = v.Logger
		} else {
			log = nil
		}
		go func(c *BasicCore, l *baseLog) {
			var rsp protocol.ExecRsp
			for !v.exited {
				rsp, err = c.Tick()
				if err != nil {
					v.errLock.Lock()
					v.emuError = err
					v.errLock.Unlock()
					close(v.rspChan)
					break
				}
				if l != nil {
					l.Log()
				}
				if rsp.Type == protocol.ExitRsp {
					v.rspChan <- rsp
					break
				} else if rsp.Type == protocol.PrintfRsp {
					v.rspChan <- rsp
				}
			}
		}(coreProc, log)
	}
	v.handleInterrupt()
	rv = v.retVal
	return rv, err
}
