package exec

import (
	"fmt"

	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/regfile"
)

// Exception is the collection of synchronous Traps from privileged Spec 1.10
type Exception uint64

// types of synchronous exceptions as of Priv spec 1.10
const (
	InstrMisaligned Exception = iota
	InstrAccessFault
	IllegalInstr
	Breakpoint
	LdAddrMisaligned
	LdAccessFault
	StAddrMisaligned
	StAccessFault
	EcallFromU
	EcallFromS
	_ // Reserved for future use
	EcallFromM
	InstrPageFault
	LdPageFault
	_ // Reserved for future use
	StPageFault
)

// SetupMagicHandler points all traps to our own universal handler
func SetupMagicHandler(coreState *SpikeExec) {
	// change state to user
	coreState.priv = UserPriv
	// set MTVEC to a RESERVED value, indicating all exceptions are handled by the emulator
	coreState.csrRegs.Write(regfile.MTVEC, 0x3)
	// do same with STVEC
	coreState.csrRegs.Write(regfile.STVEC, 0x3)
	// TODO: enable interrupts
	// enable syscalls from userspace
	enabledExceptions := uint64(0x1 << EcallFromU)
	coreState.csrRegs.Write(regfile.MEDELEG, enabledExceptions)
}

// HandleTrap performs machine-level logic for traps. CURRENTLY NO INTERRUPTS
//
// A lot of this code is based on the source of Rvemu and Spike, who have solid
// implementations, but they "cheat" by using language level exceptions instead of
// managing state using ONLY CSRs. I'm trying to do it the way it'd have to be done
// in hardware, while still giving myself the ability to offer userspace emulation.
func HandleTrap(execStage *SpikeExec, excep Exception, pc uint64) protocol.ExecRsp {
	// use MEDELEG to quickly jump to supervisor mode when possible
	if execStage.csrRegs.Read(regfile.MEDELEG)>>uint64(excep) == 0x1 {
		execStage.csrRegs.Write(regfile.SEPC, pc)
		execStage.csrRegs.Write(regfile.SCAUSE, uint64(excep))
		// TODO: Change this to only affect two bits
		execStage.csrRegs.Write(regfile.SSTATUS, uint64(execStage.priv)<<11|execStage.csrRegs.Read(regfile.SSTATUS))
		// TODO: assign STVAL according to Sec 4.1.9 of priv spec
		// Move Execution to the handler
		switch execStage.csrRegs.Read(regfile.STVEC) & 0x3 {
		case 0x0: // Direct
			npc := (execStage.csrRegs.Read(regfile.STVEC) &^ 0x3)
			return protocol.ExecRsp{Type: protocol.JmpPCRsp, Jmp: protocol.FetchJmp{
				PC: npc,
			}}
		case 0x1: // Vectored
			npc := (execStage.csrRegs.Read(regfile.STVEC) &^ 0x3) + 4*uint64(excep)
			return protocol.ExecRsp{Type: protocol.JmpPCRsp, Jmp: protocol.FetchJmp{
				PC: npc,
			}}
		default: // Go to magic handler
			execStage.priv = SupervisorPriv
			return magicSupervisorHandler(execStage, excep)
		}
	} else { // otherwise use default behavior of machine mode
		execStage.csrRegs.Write(regfile.MEPC, pc)
		execStage.csrRegs.Write(regfile.MCAUSE, uint64(excep))
		execStage.csrRegs.Write(regfile.MSTATUS, uint64(execStage.priv)<<11|execStage.csrRegs.Read(regfile.MSTATUS))
		switch execStage.csrRegs.Read(regfile.MTVAL) & 0x3 {
		case 0x0: // Direct
			npc := (execStage.csrRegs.Read(regfile.MTVEC) &^ 0x3)
			return protocol.ExecRsp{Type: protocol.JmpPCRsp, Jmp: protocol.FetchJmp{
				PC: npc,
			}}
		case 0x1: // Vectored
			npc := (execStage.csrRegs.Read(regfile.MTVEC) &^ 0x3) + 4*uint64(excep)
			return protocol.ExecRsp{Type: protocol.JmpPCRsp, Jmp: protocol.FetchJmp{
				PC: npc,
			}}
		default: // Go to magic handler
			execStage.priv = MachinePriv
			magicMachineHandler(execStage, excep)
		}
	}
	return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
}

// magicMachineHandler is an emulated interface how machine-level exceptions
func magicMachineHandler(coreState *SpikeExec, excep Exception) {
	panic("machine level exceptions are unsupported")
}

// magicSupervisorHandler is an emulated interface for supervisor-level exceptions
//
// Currently, this only handles write and exit() calls based on the riscv-pk source
func magicSupervisorHandler(coreState *SpikeExec, excep Exception) (rsp protocol.ExecRsp) {
	if excep == EcallFromU {
		switch coreState.phyRegs.Read(regfile.A7) {
		case 64: // write
			addr := coreState.phyRegs.Read(regfile.A1)
			len := coreState.phyRegs.Read(regfile.A2)
			rsp = protocol.ExecRsp{Type: protocol.PrintfRsp, Print: protocol.Printf{Addr: addr, Len: len}}
		case 93: // exit
			code := coreState.phyRegs.Read(regfile.A0)
			rsp = protocol.ExecRsp{Type: protocol.ExitRsp, Exit: protocol.Exit{Code: int(code)}}
		default:
			panic(fmt.Errorf(
				"unhandled S-Level Ecall: %#v\t@ PC\t %#x",
				coreState.phyRegs.Read(regfile.A7),
				coreState.csrRegs.Read(regfile.SEPC)),
			)
		}
	} else {
		panic("unknown exception type")
	}
	return
}
