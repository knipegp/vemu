package exec

// Extension is a collections of RISC-V instructions.
type Extension struct {
	insns []Inst
}

// Insns retrieves the instructions registered to the extension.
func (e Extension) Insns() []Inst {
	return e.insns
}
