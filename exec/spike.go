package exec

import (
	"container/list"
	"errors"
	"math"

	"gitlab.com/knipegp/vemu/bits"
	"gitlab.com/knipegp/vemu/decoder"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/regfile"
	"gitlab.com/knipegp/vemu/riscv"
)

// PrivilegeLevel type for the following enum
type PrivilegeLevel uint8

// RISC-V privilege levels for 1.10 ISA. Most emulators track this as a variable, but hardware uses MSTATUS.MPP
const (
	MachinePriv PrivilegeLevel = iota
	SupervisorPriv
	UserPriv
)

const (
	cachelen int = 8191
)

const RegLen int = 8

// SpikeExec copies the execution hashing algorithm found in the spike-isa-sim.
type SpikeExec struct {
	insnCache [cachelen]Inst
	insns     *list.List
	priv      PrivilegeLevel
	phyRegs   regfile.RegFile
	csrRegs   *regfile.CsrRegfile
	// floatRegs regfile.FloatRegFile
	signedWB bool
	wbReg    regfile.RegName
	WbIP     bool
}

// NewSpikeExec initializes the SpikeExec instance with the extensions specified by the options ExtensionConfig.
// Requires ALU to build instructions.
// Throws an error if any of the requested extensions or the architecture is unsupported.
func NewSpikeExec( //nolint:gocyclo
	xlen riscv.Xlen,
	options protocol.ExtensionConfig,
	coreID uint64) (*SpikeExec, error) {
	insnList := list.New()
	newExec := &SpikeExec{phyRegs: regfile.NewIntRegFile(xlen), csrRegs: regfile.NewCsrRegfile(coreID)}
	newExec.insns = insnList
	pushSlice(insnList, sysinsts)
	var misa uint64
	switch xlen {
	case riscv.Bit64:
		misa = 0x1
		pushSlice(insnList, RV64I(newExec).Insns())
		if (options & protocol.ExtM) > 0 {
			pushSlice(insnList, mints)
		}
		if (options & protocol.ExtA) > 0 {
			return nil, errors.New("unsupported extension")
		}
		if (options & protocol.ExtF) > 0 {
			return nil, errors.New("unsupported extension")
		}
		if (options & protocol.ExtD) > 0 {
			return nil, errors.New("unsupported extension")
		}
		if (options & protocol.ExtC) > 0 {
			return nil, errors.New("unsupported extension")
		}
	case riscv.Bit32:
		misa = 0x2
		pushSlice(insnList, RV32I(newExec).Insns())
		if (options & protocol.ExtM) > 0 {
			return nil, errors.New("unsupported extension")
		}
		if (options & protocol.ExtA) > 0 {
			return nil, errors.New("unsupported extension")
		}
		if (options & protocol.ExtF) > 0 {
			return nil, errors.New("unsupported extension")
		}
		if (options & protocol.ExtD) > 0 {
			return nil, errors.New("unsupported extension")
		}
		if (options & protocol.ExtC) > 0 {
			pushSlice(insnList, cints)
		}
	default:
		return nil, errors.New("unsupported architecture")
	}
	newExec.csrRegs.Write(regfile.MISA, misa)

	return newExec, nil
}

// push instructions to list from a slice
func pushSlice(l *list.List, insns []Inst) {
	for _, entry := range insns {
		l.PushBack(entry)
	}
}

//SliceToRegArr converts a slice to fixed-length array
func SliceToRegArr(data []byte) (ret [RegLen]byte) {
	for idx := 0; idx < len(data); idx++ {
		ret[idx] = data[idx]
	}
	return
}

func (s *SpikeExec) getInsn(insnWord uint32) (Inst, bool) {
	// From: https://github.com/riscv/riscv-isa-sim/blob/88c87dbbcf508b78ee2272528f5d404f3e5a0796/riscv/processor.cc#L1130
	idx := insnWord % uint32(cachelen)
	test := s.insnCache[idx]
	if insnWord != test.ID {
		front := s.insns.Front()
		entry := front
		var match Inst
		for ; entry != nil; entry = entry.Next() {
			match = entry.Value.(Inst)
			if insnWord&match.Mask == match.ID {
				break
			}
		}
		if entry == nil {
			return Inst{}, false
		}
		test = match
		// TODO: Deleted
		// if match.Mask != 0 && entry != front {
		// 	prev := entry.Prev().Value.(Inst)
		// 	next := entry.Next().Value.(Inst)
		// 	if match.ID != prev.ID && match.ID != next.ID {
		s.insns.MoveToFront(entry)
		// 	}
		// }
	}
	test.ID = insnWord
	s.insnCache[idx] = test
	return test, true
}

// Commit uses the instruction bits to commit state to the module.
func (s *SpikeExec) Commit(insnBits uint32, pc uint64) (rsp protocol.ExecRsp, err error) {
	insn, ok := s.getInsn(insnBits)
	if !ok {
		err = NewInsnMissing(pc, insnBits)
		return
	}
	raw := decoder.NewRawInstruction(insnBits)
	rsp = insn.Commit(raw, pc, s)
	s.commitCsrs()
	return
}

// WriteBack receives data to be written to a register file.
func (s *SpikeExec) WriteBack(data [8]byte, len uint64) {
	if s.WbIP {
		s.WbIP = false
	} else {
		panic("attempted write-back with no recorded load operation")
	}
	var rawWord uint64 = 0
	for idx := uint64(0); idx < len; idx++ {
		rawWord |= uint64(data[idx]) << (idx * 8)
	}
	var sign uint64 = 0
	if s.signedWB {
		signBit := int((len * 8) - 1)
		sign = bits.GetBits(rawWord, signBit, signBit)
	}
	var word uint64
	if s.signedWB && sign == 1 {
		var negMask uint64 = math.MaxUint64 << (len * 8)
		word = rawWord | negMask
	} else {
		word = rawWord
	}
	s.phyRegs.Write(s.wbReg, word)
}

func (s *SpikeExec) commitCsrs() {
	mcycle := s.csrRegs.Read(0xb00)
	// mcycle
	s.csrRegs.Write(0xb00, mcycle+1)
	// minstret
	minstret := s.csrRegs.Read(0xb02)
	s.csrRegs.Write(0xb02, minstret+1)
}

// String returns the current module state in a string.
func (s *SpikeExec) String() string {
	return s.phyRegs.String()
}
