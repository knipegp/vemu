package exec

import (
	"fmt"
	"math/big"

	"gitlab.com/knipegp/vemu/decoder"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/regfile"
)

var mints = []Inst{
	NewInst("mul",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Mul),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			res := rs1 * rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("mulh",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Mulh),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())))
			if mod.csrRegs.Read(regfile.MISA)&0x1 == 0 {
				fmt.Println("32-bit")
				res := uint64(rs1 * rs2 >> 32)
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			} else {
				bigrs1 := new(big.Int)
				bigrs1.SetInt64(rs1)
				bigrs2 := new(big.Int)
				bigrs2.SetInt64(rs2)
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), bigrs1.Mul(bigrs1, bigrs2).Rsh(bigrs1, 64).Uint64())
			}

			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("mulhu",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Mulhu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			if mod.csrRegs.Read(regfile.MISA)&0x1 == 0 {
				res := rs1 * rs2 >> 32
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			} else {
				bigrs1 := new(big.Int)
				bigrs1.SetUint64(rs1)
				bigrs2 := new(big.Int)
				bigrs2.SetUint64(rs2)
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), bigrs1.Mul(bigrs1, bigrs2).Rsh(bigrs1, 64).Uint64())
			}

			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("mulhsu",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Mulhsu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			if mod.csrRegs.Read(regfile.MISA)&0x1 == 0 {
				res := rs1 * int64(rs2) >> 32
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(res))
			} else {
				bigrs1 := new(big.Int)
				bigrs1.SetInt64(rs1)
				bigrs2 := new(big.Int)
				bigrs2.SetUint64(rs2)
				result := bigrs1.Mul(bigrs1, bigrs2).Rsh(bigrs1, 64).Int64()
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(result))
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("mulw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Mul),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())) & 0xFFFFFFFF)
			rs2 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0xFFFFFFFF)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(rs1*rs2))
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("div",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Div),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())))
			if rs2 == 0 || (rs1 == -(int64(^uint64(0)>>1)-1) && rs2 == -1) {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), ^uint64(0))
			} else {
				result := rs1 / rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(result))
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("divw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Div),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())) & 0xFFFFFFFF)
			rs2 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0xFFFFFFFF)
			if rs2 == 0 || (rs1 == -(int32(^uint32(0)>>1)-1) && rs2 == -1) {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), ^uint64(0))
			} else {
				result := rs1 / rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(result))
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("divu",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Divu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			if rs2 == 0 {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), ^uint64(0))
			} else {
				result := rs1 / rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), result)
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("divuw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Divu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := uint32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())) & 0xFFFFFFFF)
			rs2 := uint32(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0xFFFFFFFF)
			if rs2 == 0 {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), ^uint64(0))
			} else {
				result := rs1 / rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(int32(result)))
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("rem",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Rem),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())))
			if rs2 == 0 || (rs1 == -(int64(^uint64(0)>>1)-1) && rs2 == -1) {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(rs1))
			} else {
				result := rs1 % rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(result))
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("remw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Rem),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())) & 0xFFFFFFFF)
			rs2 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0xFFFFFFFF)
			if rs2 == 0 || (rs1 == -(int32(^uint32(0)>>1)-1) && rs2 == -1) {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(rs1))
			} else {
				result := rs1 % rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(result))
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("remu",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Remu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			if rs2 == 0 {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), rs1)
			} else {
				result := rs1 % rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), result)
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("remuw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithFunct7(decoder.MulDiv).
			WithStdFunct3(decoder.Remu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := uint32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())) & 0xFFFFFFFF)
			rs2 := uint32(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0xFFFFFFFF)
			if rs2 == 0 {
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(int32(rs1)))
			} else {
				result := rs1 % rs2
				mod.phyRegs.Write(regfile.RegName(inst.GetRd()), uint64(int32(result)))
			}
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
}
