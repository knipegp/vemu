package exec

import (
	"math"

	"gitlab.com/knipegp/vemu/bits"
	"gitlab.com/knipegp/vemu/decoder"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/regfile"
)

var rviBase = []Inst{
	NewInst("lui",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Lui),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			res := uint64(int32(inst.GetImmediate()))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("ecall",
		decoder.IDBuilder{}.WithCustom(0x73, 0xffffffff),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			return HandleTrap(mod, EcallFromU, pc)
		},
	),
	NewInst("add",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithStdFunct3(decoder.RegDiff).
			WithFunct7(decoder.Add),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			res := rs1 + rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sub",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithStdFunct3(decoder.RegDiff).
			WithFunct7(decoder.Sub),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			res := rs1 - rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("and",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg).WithStdFunct3(decoder.And).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			res := rs1 & rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("or",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg).WithStdFunct3(decoder.Or).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			res := rs1 | rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("xor",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg).WithStdFunct3(decoder.Xor).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			res := rs1 ^ rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("slt",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg).WithStdFunct3(decoder.Slt).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var res uint64
			if int64(rs1) < int64(rs2) {
				res = 1
			} else {
				res = 0
			}
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sltu",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg).WithStdFunct3(decoder.Sltu).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var res uint64
			if rs1 < rs2 {
				res = 1
			} else {
				res = 0
			}
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("beq",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Branch).WithStdFunct3(decoder.Beq),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var execRsp protocol.ExecRsp
			if rs1 == rs2 {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.GetImmediate()), 11)
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("bgeu",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Branch).WithStdFunct3(decoder.Bgeu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var execRsp protocol.ExecRsp
			if rs1 >= rs2 {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.GetImmediate()), 11)
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("bge",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Branch).WithStdFunct3(decoder.Bge),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var execRsp protocol.ExecRsp
			if int64(rs1) >= int64(rs2) {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.GetImmediate()), 11)
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("bne",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Branch).WithStdFunct3(decoder.Bne),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var execRsp protocol.ExecRsp
			if rs1 != rs2 {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.GetImmediate()), 11)
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("blt",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Branch).WithStdFunct3(decoder.Blt),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var execRsp protocol.ExecRsp
			if int64(rs1) < int64(rs2) {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.GetImmediate()), 11)
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("bltu",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Branch).WithStdFunct3(decoder.Bltu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			var execRsp protocol.ExecRsp
			if rs1 < rs2 {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.GetImmediate()), 11)
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("addi",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Addi),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 + imm
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("xori",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Xori),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 ^ imm
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("slti",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Slti),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			var res uint64
			if int64(rs1) < int64(imm) {
				res = 1
			} else {
				res = 0
			}
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sltiu",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Sltiu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			var res uint64
			if rs1 < imm {
				res = 1
			} else {
				res = 0
			}
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("andi",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Andi),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 & imm
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("ori",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Ori),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 | imm
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("jal",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Jal),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), pc+4)
			var execRsp protocol.ExecRsp
			execRsp.Type = protocol.JmpPCRsp
			execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.GetImmediate()), 20)
			return execRsp
		},
	),
	NewInst("jalr",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Jalr),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), pc+4)
			var execRsp protocol.ExecRsp
			execRsp.Type = protocol.JmpPCRsp
			execRsp.Jmp.PC = rs1 + bits.SignExt(uint64(inst.GetImmediate()), 11)
			return execRsp
		},
	),
	NewInst("fence.i",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.FenceOp).WithStdFunct3(decoder.Fencei),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			// TODO: Implement if instructions can be fetched before prev
			// instructions are committed. For same HART.
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sw",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Store).WithStdFunct3(decoder.Sw),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			addr := rs1 + imm
			data := bits.Uint32ByteArray(uint32(rs2))
			return protocol.ExecRsp{Type: protocol.ExecMemWrite, Write: protocol.MemWrite{Addr: addr, Data: data, Len: 4}}
		},
	),
	NewInst("sh",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Store).WithStdFunct3(decoder.Sh),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			addr := rs1 + imm
			data := bits.Uint16ByteArray(uint16(rs2))
			return protocol.ExecRsp{Type: protocol.ExecMemWrite, Write: protocol.MemWrite{Addr: addr, Data: data, Len: 2}}
		},
	),
	NewInst("sb",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Store).WithStdFunct3(decoder.Sb),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			addr := rs1 + imm
			data := [8]byte{byte(rs2)}
			return protocol.ExecRsp{Type: protocol.ExecMemWrite, Write: protocol.MemWrite{Addr: addr, Data: data, Len: 1}}
		},
	),
	NewInst("lw",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Load).WithStdFunct3(decoder.Lw),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			addr := rs1 + imm
			mod.signedWB = true
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.WbIP = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 4}}
		},
	),
	NewInst("lh",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Load).WithStdFunct3(decoder.Lh),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			addr := rs1 + imm
			mod.signedWB = true
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.WbIP = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 2}}
		},
	),
	NewInst("lb",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Load).WithStdFunct3(decoder.Lb),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			addr := rs1 + imm
			mod.signedWB = true
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.WbIP = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 1}}
		},
	),
	NewInst("lhu",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Load).WithStdFunct3(decoder.Lhu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			addr := rs1 + imm
			mod.signedWB = false
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.WbIP = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 2}}
		},
	),
	NewInst("lbu",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Load).WithStdFunct3(decoder.Lbu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			addr := rs1 + imm
			mod.signedWB = false
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.WbIP = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 1}}
		},
	),
}

var rv32i = []Inst{
	NewInst("auipc",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Auipc),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			res := pc + uint64(inst.GetImmediate())
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srai",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegImm).
			WithStdFunct3(decoder.Sr).
			WithCustom(16, 0xfc000000),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 >> (imm & 0x1f)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srli",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegImm).
			WithStdFunct3(decoder.Sr).
			WithCustom(0, 0xfc000000),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			// TODO: Not portable to 64-bit
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1())) & math.MaxUint32
			res := rs1 >> (imm & 0x1f)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("slli",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Slli),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := uint64(inst.GetImmediate())
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 << (imm & 0x1f)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sll",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg).WithStdFunct3(decoder.Sll).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x1f
			res := rs1 << rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sra",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithStdFunct3(decoder.Sr).
			WithFunct7(decoder.Sra),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x1f
			// TODO: Not portable to rv64
			res := uint64(int32(rs1) >> rs2)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srl",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithStdFunct3(decoder.Sr).
			WithFunct7(decoder.Srl),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x1f
			res := uint64(uint32(rs1) >> rs2)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
}

// RV32I retrieves the base instructions for I32.
func RV32I(mod *SpikeExec) *Extension {
	insns := append(rviBase, rv32i...)
	return &Extension{insns}
}

var rv64i = []Inst{
	NewInst("auipc",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Auipc),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			res := pc + uint64(int32(inst.GetImmediate()))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srai",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegImm).
			WithStdFunct3(decoder.Sr).
			WithCustom(16, 0xfc000000),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := int64(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			res := uint64(rs1 >> (imm & 0x3f))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srli",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegImm).
			WithStdFunct3(decoder.Sr).
			WithCustom(0, 0xfc000000),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 >> (imm & 0x3f)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("slli",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm).WithStdFunct3(decoder.Slli),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := uint64(inst.GetImmediate())
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			res := rs1 << (imm & 0x3f)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sraiw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegImm32).
			WithStdFunct3(decoder.Sr).
			WithCustom(16, 0xfc000000),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			res := uint64(rs1 >> (imm & 0x1f))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srliw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegImm32).
			WithStdFunct3(decoder.Sr).
			WithCustom(0, 0xfc000000),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := uint32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			res := uint64(int32(rs1 >> (imm & 0x1f)))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("slliw",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm32).WithStdFunct3(decoder.Slli),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := uint64(inst.GetImmediate())
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			res := uint64(rs1 << (imm & 0x1f))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("addiw",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegImm32).WithStdFunct3(decoder.Addi),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := int32(bits.SignExt(uint64(inst.GetImmediate()), 11))
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			res := uint64(rs1 + imm)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sd",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Store).WithStdFunct3(decoder.Sd),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2()))
			addr := rs1 + imm
			data := bits.Uint64ByteArray(rs2)
			return protocol.ExecRsp{Type: protocol.ExecMemWrite, Write: protocol.MemWrite{Addr: addr, Data: data, Len: 8}}
		},
	),
	NewInst("ld",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Load).WithStdFunct3(decoder.Ld),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			addr := rs1 + imm
			mod.WbIP = true
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.signedWB = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 8}}
		},
	),
	NewInst("lwu",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Load).WithStdFunct3(decoder.Lwu),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			imm := bits.SignExt(uint64(inst.GetImmediate()), 11)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			addr := rs1 + imm
			mod.WbIP = true
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.signedWB = false
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 4}}
		},
	),
	NewInst("addw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithStdFunct3(decoder.RegDiff).
			WithFunct7(decoder.Add),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())))
			res := uint64(rs1 + rs2)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("subw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithStdFunct3(decoder.RegDiff).
			WithFunct7(decoder.Sub),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs2())))
			res := uint64(rs1 - rs2)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sllw",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg32).WithStdFunct3(decoder.Sll),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x1f
			res := uint64(rs1 << rs2)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srlw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithStdFunct3(decoder.Sr).
			WithFunct7(decoder.Srl),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := uint32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x1f
			res := uint64(int32(rs1 >> rs2))
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sraw",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg32).
			WithStdFunct3(decoder.Sr).
			WithFunct7(decoder.Sra),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := int32(mod.phyRegs.Read(regfile.RegName(inst.GetRs1())))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x1f
			res := uint64(rs1 >> rs2)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sll",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.IntRegReg).WithStdFunct3(decoder.Sll).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x3f
			res := rs1 << rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("sra",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithStdFunct3(decoder.Sr).
			WithFunct7(decoder.Sra),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x3f
			res := uint64(int64(rs1) >> rs2)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("srl",
		decoder.IDBuilder{}.
			WithMajOp(decoder.Std).
			WithMinOp(decoder.IntRegReg).
			WithStdFunct3(decoder.Sr).
			WithFunct7(decoder.Srl),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.GetRs2())) & 0x3f
			res := rs1 >> rs2
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
}

// RV64I retrieves the base instructions for I64.
func RV64I(mod *SpikeExec) *Extension {
	insns := append(rviBase, rv64i...)
	return &Extension{insns}
}
