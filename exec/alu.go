package exec

import (
	"fmt"
	"gitlab.com/knipegp/vemu/riscv"
)

// TODO: Rather than just using the logic unit to sign extend, we should explore
// having each basic arithmetic operation as a method. Add, sub, xor, and etc...

// LogicUnit provides methods to supplement native arithmetic operations.
type LogicUnit interface {
	CPI() uint64
}

type baseAlu struct {
	cpi uint64
}

// CPI retrieves the cycles per instruction for the logic unit.
func (b baseAlu) CPI() uint64 {
	return b.cpi
}

// ALU32 provides arithmetic operations for the specified bit length operands.
type ALU32 struct {
	baseAlu
}

// ALU64 provides arithmetic operations for the specified bit length operands.
type ALU64 struct {
	baseAlu
}

// NewALU initializes an ALU
func NewALU(xlen riscv.Xlen, cpi uint64) LogicUnit {
	base := baseAlu{cpi}
	switch xlen {
	case riscv.Bit32:
		return ALU32{base}
	case riscv.Bit64:
		return ALU64{base}
	default:
		panic(fmt.Errorf("unknown xlen, %v", xlen))
	}
}
