package exec

import (
	"gitlab.com/knipegp/vemu/decoder"
	"gitlab.com/knipegp/vemu/protocol"
)

// Inst contains methods for modifying emulator state for a given instruction
type Inst struct {
	Name string
	Mask uint32
	ID   uint32
	impl func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp
}

// NewInst returns a new instruction implementation.
func NewInst(
	name string,
	id decoder.IDBuilder,
	impl func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp) Inst {
	return Inst{Name: name, ID: id.ID(), Mask: id.Mask(), impl: impl}
}

// Commit updates the emulator state based on the instruction implementation.
func (i Inst) Commit(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
	return i.impl(inst, pc, mod)
}
