package exec

import (
	"math"

	"gitlab.com/knipegp/vemu/bits"
	"gitlab.com/knipegp/vemu/decoder"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/regfile"
)

var cints = []Inst{
	NewInst("c.lw",
		decoder.IDBuilder{}.WithMajOp(decoder.C0).WithCFunct3(decoder.Clw),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			addr := uint64(inst.CWImm()) + mod.phyRegs.Read(regfile.RegName(inst.CRdRs1Short()))
			mod.signedWB = true
			mod.wbReg = regfile.RegName(inst.CRdShort())
			mod.WbIP = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 4}}
		},
	),
	NewInst("c.sw",
		decoder.IDBuilder{}.WithMajOp(decoder.C0).WithCFunct3(decoder.Csw),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			addr := uint64(inst.CWImm()) + mod.phyRegs.Read(regfile.RegName(inst.CRdRs1Short()))
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.CRs2Short()))
			data := bits.Uint32ByteArray(uint32(rs2))
			return protocol.ExecRsp{Type: protocol.ExecMemWrite, Write: protocol.MemWrite{Addr: addr, Data: data, Len: 4}}
		},
	),
	NewInst("c.li",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Cli),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			res := bits.SignExt(uint64(inst.CIImm()), 5)
			mod.phyRegs.Write(regfile.RegName(inst.CRdRs1()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.lui",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Clui),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			// Implements both Lui and Addi16sp because of some decoding voodoo
			rd := inst.CRdRs1()
			var res uint64
			if rd == 2 {
				res = bits.SignExt(uint64(inst.CADDI16Imm()), 9) + mod.phyRegs.Read(regfile.Sp)
			} else {
				res = bits.SignExt(uint64(inst.CLUIImm()), 17)
			}
			mod.phyRegs.Write(regfile.RegName(rd), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.addi4spn",
		decoder.IDBuilder{}.WithMajOp(decoder.C0).WithCFunct3(decoder.Caddi4spn),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			sp := mod.phyRegs.Read(regfile.Sp)
			res := sp + uint64(inst.CADDI4Imm())
			mod.phyRegs.Write(regfile.RegName(inst.CRdShort()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.slli",
		decoder.IDBuilder{}.WithMajOp(decoder.C2).WithCFunct3(decoder.Cslli),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.CRdRs1()))
			res := rs1 << uint64(inst.CIImm())
			mod.phyRegs.Write(regfile.RegName(inst.CRdRs1()), res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.srai",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Extend6).WithCFunct6Tail(decoder.Csrai),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			srcDst := regfile.RegName(inst.CRdRs1Short())
			src := int64(mod.phyRegs.Read(srcDst))
			res := uint64(src >> uint64(inst.CIImm()))
			mod.phyRegs.Write(srcDst, res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.srli",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Extend6).WithCFunct6Tail(decoder.Csrli),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			srcDst := regfile.RegName(inst.CRdRs1Short())
			src := uint32(mod.phyRegs.Read(srcDst))
			res := uint64(src >> uint64(inst.CIImm()))
			mod.phyRegs.Write(srcDst, res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.mv",
		decoder.IDBuilder{}.WithMajOp(decoder.C2).WithCFunct3(decoder.CMvAdd).WithCBit12(decoder.Cmv),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs2Name := regfile.RegName(inst.CRs2())
			rs1Name := regfile.RegName(inst.CRdRs1())
			// TODO: This is the c.jr pseudo-instruction. It's confusing to
			// implement it like this. There has to be a better way to
			// handle this.
			var execRsp protocol.ExecRsp
			if rs2Name == regfile.Zero {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = mod.phyRegs.Read(rs1Name)
			} else {
				rs2 := mod.phyRegs.Read(regfile.RegName(inst.CRs2()))
				mod.phyRegs.Write(rs1Name, rs2)
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("c.add",
		decoder.IDBuilder{}.WithMajOp(decoder.C2).WithCFunct3(decoder.CMvAdd).WithCBit12(decoder.Cadd),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.CRdRs1()))
			rs2Name := regfile.RegName(inst.CRs2())
			// Jalr
			var execRsp protocol.ExecRsp
			if rs2Name == regfile.Zero {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = rs1
				mod.phyRegs.Write(regfile.Ra, pc+2)
			} else {
				rs2 := mod.phyRegs.Read(rs2Name)
				res := rs1 + rs2
				mod.phyRegs.Write(regfile.RegName(inst.CRdRs1()), res)
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("c.addi",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Caddi),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rdName := regfile.RegName(inst.CRdRs1())
			rs1 := mod.phyRegs.Read(rdName)
			imm := bits.SignExt(uint64(inst.CIImm()), 5)
			res := imm + rs1
			mod.phyRegs.Write(rdName, res)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.andi",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Extend6).WithCFunct6Tail(decoder.Candi),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rdName := regfile.RegName(inst.CRdRs1Short())
			rs1 := mod.phyRegs.Read(rdName)
			imm := bits.SignExt(uint64(inst.CIImm()), 5)
			mod.phyRegs.Write(rdName, imm&rs1)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.sub",
		decoder.IDBuilder{}.
			WithMajOp(decoder.C1).
			WithCFunct3(decoder.Extend6).
			WithCBit12(decoder.RegGroupMid).
			WithCFunct6Tail(decoder.RegGroupTail).
			WithCFunct2(decoder.Csub),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rdName := regfile.RegName(inst.CRdRs1Short())
			rs1 := mod.phyRegs.Read(rdName)
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.CRs2Short()))
			mod.phyRegs.Write(rdName, rs1-rs2)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.xor",
		decoder.IDBuilder{}.
			WithMajOp(decoder.C1).
			WithCFunct3(decoder.Extend6).
			WithCBit12(decoder.RegGroupMid).
			WithCFunct6Tail(decoder.RegGroupTail).
			WithCFunct2(decoder.Cxor),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rdName := regfile.RegName(inst.CRdRs1Short())
			rs1 := mod.phyRegs.Read(rdName)
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.CRs2Short()))
			mod.phyRegs.Write(rdName, rs1^rs2)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.and",
		decoder.IDBuilder{}.
			WithMajOp(decoder.C1).
			WithCFunct3(decoder.Extend6).
			WithCBit12(decoder.RegGroupMid).
			WithCFunct6Tail(decoder.RegGroupTail).
			WithCFunct2(decoder.Cand),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rdName := regfile.RegName(inst.CRdRs1Short())
			rs1 := mod.phyRegs.Read(rdName)
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.CRs2Short()))
			mod.phyRegs.Write(rdName, rs1&rs2)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.or",
		decoder.IDBuilder{}.
			WithMajOp(decoder.C1).
			WithCFunct3(decoder.Extend6).
			WithCBit12(decoder.RegGroupMid).
			WithCFunct6Tail(decoder.RegGroupTail).
			WithCFunct2(decoder.Cor),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rdName := regfile.RegName(inst.CRdRs1Short())
			rs1 := mod.phyRegs.Read(rdName)
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.CRs2Short()))
			mod.phyRegs.Write(rdName, rs1|rs2)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("c.jal",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Cjal),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			mod.phyRegs.Write(regfile.Ra, pc+2)
			var execRsp protocol.ExecRsp
			execRsp.Type = protocol.JmpPCRsp
			execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.CJImm()), 11)
			return execRsp
		},
	),
	NewInst("c.j",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Cj),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			var execRsp protocol.ExecRsp
			execRsp.Type = protocol.JmpPCRsp
			execRsp.Jmp.PC = pc + bits.SignExt(uint64(inst.CJImm()), 11)
			return execRsp
		},
	),
	NewInst("c.beqz",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Cbeqz),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.CRdRs1Short()))
			var execRsp protocol.ExecRsp
			if rs1 == 0 {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = bits.SignExt(uint64(inst.CBImm()), 8) + pc
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("c.bnez",
		decoder.IDBuilder{}.WithMajOp(decoder.C1).WithCFunct3(decoder.Cbnez),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.CRdRs1Short()))
			var execRsp protocol.ExecRsp
			if rs1 != 0 {
				execRsp.Type = protocol.JmpPCRsp
				execRsp.Jmp.PC = bits.SignExt(uint64(inst.CBImm()), 8) + pc
			} else {
				execRsp.Type = protocol.EmptyExecRsp
			}
			return execRsp
		},
	),
	NewInst("c.swsp",
		decoder.IDBuilder{}.WithMajOp(decoder.C2).WithCFunct3(decoder.Cswsp),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			rs2 := mod.phyRegs.Read(regfile.RegName(inst.CRs2()))
			addr := mod.phyRegs.Read(regfile.Sp) + uint64(inst.CSWSPImm())
			data := bits.Uint32ByteArray(uint32(rs2))
			return protocol.ExecRsp{Type: protocol.ExecMemWrite, Write: protocol.MemWrite{Addr: addr, Data: data, Len: 4}}
		},
	),
	NewInst("c.lwsp",
		decoder.IDBuilder{}.WithMajOp(decoder.C2).WithCFunct3(decoder.Clwsp),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			addr := (mod.phyRegs.Read(regfile.Sp) + uint64(inst.CLWSPImm())) & math.MaxUint32
			mod.signedWB = true
			mod.wbReg = regfile.RegName(inst.GetRd())
			mod.WbIP = true
			return protocol.ExecRsp{Type: protocol.ExecMemRead, Read: protocol.MemRead{Addr: addr, Len: 4}}
		},
	),
}
