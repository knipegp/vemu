package exec

import "fmt"

// InsnMissing indicates that the requested instruction is not implemented in
// the pipeline.
type InsnMissing struct {
	PC   uint64
	Bits uint32
}

func NewInsnMissing(pc uint64, bits uint32) *InsnMissing {
	return &InsnMissing{
		PC:   pc,
		Bits: bits,
	}
}

// Error returns the error message.
func (i *InsnMissing) Error() string {
	return fmt.Sprintf("invalid instruction %#x @%#x", i.Bits, i.PC)
}
