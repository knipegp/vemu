package exec

import (
	"gitlab.com/knipegp/vemu/decoder"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/regfile"
)

var sysinsts = []Inst{
	NewInst("csrrs",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Sys).WithStdFunct3(decoder.Csrrs),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			csrIdx := int(inst.GetImmediate())
			initCsr := mod.csrRegs.Read(csrIdx)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			mod.csrRegs.Write(csrIdx, initCsr|rs1)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), initCsr)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("csrrw",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Sys).WithStdFunct3(decoder.Csrrw),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			csrIdx := int(inst.GetImmediate())
			initCsr := mod.csrRegs.Read(csrIdx)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), initCsr)
			rs1 := mod.phyRegs.Read(regfile.RegName(inst.GetRs1()))
			mod.csrRegs.Write(csrIdx, rs1)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("csrrwi",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Sys).WithStdFunct3(decoder.Csrrwi).WithFunct7(0),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			csrIdx := int(inst.GetImmediate())
			initVal := mod.csrRegs.Read(csrIdx)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), initVal)
			mod.csrRegs.Write(csrIdx, uint64(inst.GetRs1()))
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
	NewInst("csrrci",
		decoder.IDBuilder{}.WithMajOp(decoder.Std).WithMinOp(decoder.Sys).WithStdFunct3(decoder.Csrrci),
		func(inst decoder.RawInstruction, pc uint64, mod *SpikeExec) protocol.ExecRsp {
			csrIdx := int(inst.GetImmediate())
			initVal := mod.csrRegs.Read(csrIdx)
			mod.phyRegs.Write(regfile.RegName(inst.GetRd()), initVal)
			rs1 := uint64(inst.GetRs1())
			mod.csrRegs.Write(csrIdx, initVal&^rs1)
			return protocol.ExecRsp{Type: protocol.EmptyExecRsp}
		},
	),
}
