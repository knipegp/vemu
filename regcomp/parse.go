package regcomp

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"regexp"
)

// Creates submatches for all register state print-outs in a register log.
const pattern = `(?:^|\n)([rfsgtap][acp\d]+)\s+0x([[:xdigit:]]+)`

func getmatches(filepath string) [][]string {
	filebytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		panic(err)
	}
	exp, err := regexp.Compile(pattern)
	if err != nil {
		panic(err)
	}
	log := string(filebytes)
	matches := exp.FindAllStringSubmatch(log, -1)
	if len(matches)%32 != 0 {
		panic(fmt.Errorf("incomplete log file"))
	}
	return matches
}

func parsematch(match []string, expectreg regname) uint64 {
	if expectreg != regname(match[1]) {
		panic(fmt.Errorf("register name is not consistent"))
	}
	hexstr := match[2]
	var hexstring string
	// DecodeString expects even length hex string
	if len(hexstr)%2 != 0 {
		hexstring = "0" + hexstr
	} else {
		hexstring = hexstr
	}
	val, err := hex.DecodeString(hexstring)
	if err != nil {
		panic(err)
	}
	// Uint32 expects []byte length 4
	var byte32 []byte
	if len(val) < 4 {
		byte32 = append(make([]byte, 4-len(val)), val...)
	} else {
		byte32 = val
	}
	return uint64(binary.BigEndian.Uint32(byte32))
}

func getstate(matchlist [][]string) (regvals [32]uint64) {
	if len(matchlist) != 32 {
		panic(fmt.Errorf("expected 32 register matches, got %d", len(matchlist)))
	}
	baseregs := newbaseregs()
	for idx, reg := range baseregs.regs {
		regvals[idx] = parsematch(matchlist[idx], reg)
	}
	return regvals
}

// Getallstates returns all register states in a register log file.
func Getallstates(filepath string) [][32]uint64 {
	matches := getmatches(filepath)
	statecnt := len(matches) / 32
	regvals := make([][32]uint64, statecnt)
	for idx := 0; idx < len(matches); idx += 32 {
		onestatematches := matches[idx : idx+32]
		regvals[idx/32] = getstate(onestatematches)
	}
	return regvals
}
