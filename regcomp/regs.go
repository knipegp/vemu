package regcomp

type regname string

const (
	ra  regname = "ra"
	sp  regname = "sp"
	gp  regname = "gp"
	tp  regname = "tp"
	t0  regname = "t0"
	t1  regname = "t1"
	t2  regname = "t2"
	fp  regname = "fp"
	s1  regname = "s1"
	a0  regname = "a0"
	a1  regname = "a1"
	a2  regname = "a2"
	a3  regname = "a3"
	a4  regname = "a4"
	a5  regname = "a5"
	a6  regname = "a6"
	a7  regname = "a7"
	s2  regname = "s2"
	s3  regname = "s3"
	s4  regname = "s4"
	s5  regname = "s5"
	s6  regname = "s6"
	s7  regname = "s7"
	s8  regname = "s8"
	s9  regname = "s9"
	s10 regname = "s10"
	s11 regname = "s11"
	t3  regname = "t3"
	t4  regname = "t4"
	t5  regname = "t5"
	t6  regname = "t6"
	pc  regname = "pc"
)

type baseregs struct {
	regs [32]regname
}

func newbaseregs() baseregs {
	newregs := [32]regname{ra,
		sp,
		gp,
		tp,
		t0,
		t1,
		t2,
		fp,
		s1,
		a0,
		a1,
		a2,
		a3,
		a4,
		a5,
		a6,
		a7,
		s2,
		s3,
		s4,
		s5,
		s6,
		s7,
		s8,
		s9,
		s10,
		s11,
		t3,
		t4,
		t5,
		t6,
		pc}
	return baseregs{newregs}
}
