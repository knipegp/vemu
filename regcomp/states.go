package regcomp

import (
	"fmt"
	"strings"
)

// Update is a tuple containing state updates.
type Update struct {
	RegIdx int
	NewVal uint64
}

// Print returns a string representation of the update.
func (u Update) Print() string {
	regnames := newbaseregs()
	return fmt.Sprintf("%s: 0x%x", regnames.regs[u.RegIdx], u.NewVal)
}

// UpComp compares a test update to a baseline update.
type UpComp struct {
	test []Update
	base []Update
}

// WithBase assigns the baseline update to the comparison.
func (u UpComp) WithBase(base []Update) UpComp {
	u.base = base
	return u
}

// WithTest assigns the test update to the comparison.
func (u UpComp) WithTest(test []Update) UpComp {
	u.test = test
	return u
}

// Diff contains a list of updates that do not match the base case.
type Diff struct {
	List  []Update
	Major bool
}

// Print returns a string representation of the struct.
func (d Diff) Print() string {
	var build strings.Builder
	for _, entry := range d.List {
		build.WriteString(fmt.Sprintf("%s ", entry.Print()))
	}
	return build.String()
}

// GetDiff returns the test updates that are different from the baseline.
// Returns true if the PCs are different or the number of updates differ.
func (u UpComp) GetDiff() (diff Diff) {
	if len(u.base) != len(u.test) {
		diff.List = u.test
		diff.Major = true
	} else {
		for idx := range u.test {
			if u.test[idx] != u.base[idx] {
				diff.List = append(diff.List, u.test[idx])
				// Check if the PCs are different
				if u.test[idx].RegIdx == 31 {
					diff.Major = true
				}
			}
		}
	}
	return diff
}

// GetUpdate returns the update at the given PC idx.
func GetUpdate(states [][32]uint64, pcIdx int) (all []Update) {
	if pcIdx == 0 {
		for idx, entry := range states[0] {
			all = append(all, Update{idx, entry})
		}
		return all
	}
	old := states[pcIdx-1]
	update := states[pcIdx]
	for idx := range old {
		if old[idx] != update[idx] {
			all = append(all, Update{idx, update[idx]})
		}
	}
	return all
}
