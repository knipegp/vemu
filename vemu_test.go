package main

import (
	"testing"

	"gitlab.com/akita/mem"
	"gitlab.com/knipegp/vemu/riscv"
	"gitlab.com/knipegp/vemu/vemulator"
)

func BenchmarkMtMatMul1(b *testing.B) {
	vemu := vemulator.Builder{}.
		WithXlen(riscv.Bit32).
		WithElf("./test/riscv-tests/benchmarks/mt-matmul.riscv").
		WithLog(false).
		WithMemCap(4 * mem.GB).
		WithCoreCount(1).
		Build()
	vemu.Run()
}
