package fetch

import (
	"fmt"

	"gitlab.com/knipegp/vemu/bits"
	"gitlab.com/knipegp/vemu/decoder"
	"gitlab.com/knipegp/vemu/memory"
	"gitlab.com/knipegp/vemu/protocol"
	"gitlab.com/knipegp/vemu/riscv"
)

// BasicFetch retrieves instruction bits for the current PC.
type BasicFetch struct {
	npc    uint64
	icache memory.Cache32
	reqPC  uint64
}

// NewBasicFetch returns a new instance of BasicFetch.
func NewBasicFetch(initPC uint64, xlen riscv.Xlen) BasicFetch {
	return BasicFetch{npc: initPC, icache: memory.NewCache32(xlen)}
}

func (b *BasicFetch) getBits(pc uint64) (rsp protocol.FetchRsp) {
	b.reqPC = pc
	pcPtr := pc
	var insnBits uint32
	var readOk bool
	var lsb, msb uint16
	if lsb, readOk = b.icache.Read16(pcPtr); readOk {
		pcPtr += 2
		majOp := decoder.MajorOpcode(bits.GetBits(uint64(lsb), 1, 0))
		insnBits = uint32(lsb)
		if majOp >= decoder.Std {
			msb, readOk = b.icache.Read16(pcPtr)
			if readOk {
				pcPtr += 2
				insnBits |= (uint32(msb) << 16)
			}
		}
	}
	if readOk {
		rsp.Type = protocol.FetchCommit
		rsp.Commit.InsnWord = insnBits
		rsp.Commit.PC = pc
		b.npc = pcPtr
	} else {
		rsp.Type = protocol.FetchMemRead
		rsp.Read.Addr = pcPtr & memory.RowAlignMask
		rsp.Read.Len = memory.RowBytes
		b.npc = pc
	}
	return
}

// GetBitsNPC returns the instruction word at the next stored PC.
func (b *BasicFetch) GetBitsNPC() protocol.FetchRsp {
	return b.getBits(b.npc)
}

// GetBitsJmp returns the instruction word at the passed target PC.
func (b *BasicFetch) GetBitsJmp(target uint64) protocol.FetchRsp {
	return b.getBits(target)
}

// Populate fills a cache line in the ICache.
func (b *BasicFetch) Populate(addr uint64, data [memory.RowBytes]byte) {
	b.icache.Populate(addr, data)
}

// String returns the module current state.
func (b *BasicFetch) String() string {
	return fmt.Sprintf("pc  %#x", b.reqPC)
}
