module gitlab.com/knipegp/vemu

go 1.13

require (
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/cweill/gotests v1.5.3 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/go-delve/delve v1.5.0 // indirect
	github.com/golang/mock v1.4.4
	github.com/kr/pretty v0.2.1
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/motemen/go-quickfix v0.0.0-20200118031250-2a6e54e79a50 // indirect
	github.com/motemen/gore v0.5.0 // indirect
	github.com/nbutton23/zxcvbn-go v0.0.0-20180912185939-ae427f1e4c1d // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/onsi/ginkgo v1.14.1
	github.com/onsi/gomega v1.10.2
	github.com/rogpeppe/godef v1.1.2 // indirect
	gitlab.com/akita/akita v1.10.0
	gitlab.com/akita/mem v1.8.1
	gitlab.com/akita/util v0.6.4 // indirect
	golang.org/x/sys v0.0.0-20200915084602-288bc346aa39 // indirect
	golang.org/x/tools/gopls v0.5.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	rsc.io/quote/v3 v3.1.0 // indirect
)
